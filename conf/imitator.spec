Name: imitator
Version: 0.4.1
Release: eter1

Summary: Imitator of digital interface
Summary(ru_RU.UTF-8): Имитатор цифровых интерфейсов

License: GPL
Group: Development/C++
# URL: http://sourceforge.net/uniset
Packager: Pavel Vainerman <pv@altlinux.ru>
Source: %name-%version.tar

# Automatically added by buildreq on Fri Aug 06 2010
BuildRequires: doxygen glibc-devel libglademm-devel libuniset-devel


%description
Imitator for digital interfaces imitation

%description -l ru_RU.UTF-8
Программа для имитации цифровых интерфейсов

%prep
%setup

%build
%autoreconf
%configure --with-confdir
%make_build

%install
%makeinstall_std

mkdir -p -m755 %buildroot/var/local/{log/%name,run/%name,lock,omniORB}/

mkdir -p -m755 %buildroot%_initdir/
mv -f %buildroot%_bindir/%{name}d %buildroot%_initdir/%{name}d

%post

%files
%_bindir/*
%_docdir/%name-%version
%attr(0777,root,root) %dir /var/local/log/%name
%attr(0777,root,root) %dir /var/local/run/%name
%attr(0777,root,root) %dir /var/local/lock/
%attr(0777,root,root) %dir /var/local/omniORB/


%dir %_sysconfdir/%name/
#%_sysconfdir/%name/VERSION
%_initdir/%{name}d
%config %attr(0600,root,root) %_sysconfdir/%name/%name.monitrc
%config %_sysconfdir/%name/imitator.xml
%config %_sysconfdir/%name/*-log

%dir %_datadir/%name/
%_datadir/%name/*.glade
%_datadir/%name/*.pdf
%_datadir/%name/misc/*

#%doc NEWS README TODO


%changelog
* Fri Aug 06 2010 Vitaly Lipatov <lav@altlinux.ru> 0.4.1-eter1
- fix build with new UniSet 0.98

* Fri Jun 01 2007 Pavel Vainerman <pv@altlinux.ru> 0.4.0-alt5
- new build
:q

* Sat May 14 2005 Pavel Vainerman <pv@altlinux.ru> 0.3.3-alt1
- update gui design
- fixed bug

* Thu Feb 03 2005 Pavel Vainerman <pv@altlinux.ru> 0.0.1-alt3
- add logdir
- set permission for logdir

* Sat Jan 29 2005 Pavel Vainerman <pv@altlinux.ru> 0.0.1-alt2
- update specfile

* Sat Jan 29 2005 Pavel Vainerman <pv@altlinux.ru> 0.0.1-alt1
- first build

