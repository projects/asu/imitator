#!/bin/sh
# Вспомогательный скрипт для подготовки и сборки rpm-пакета с системой
. /etc/rpm/etersoft-build-functions

# с параметром -r собираем наружу
[ "$1" = "-r" ] && REMOTEBUILD=1

# с параметром -l собираем прямо из каталога (без CVS)
[ "$1" = "-l" ] && LOCALBUILD=local

SPECNAME=conf/imitator.spec

cd ..

WORKDIR=$(pwd)

if [ -z "$LOCALBUILD" ] ; then
	check_key
	update_from_cvs
fi

build_rpms_name $SPECNAME
export BUILDNAME=$BASENAME-$VERSION-$RELEASE

if [ -z "$LOCALBUILD" ] ; then
	cvs update $SPECNAME ChangeLog && ./cvs2cl.pl && cvs commit -m "Auto updated by $0 for $BUILDNAME" ChangeLog || fatal "can't update Changelog"
fi

add_changelog_helper "- new build" $SPECNAME

prepare_tarball $LOCALBUILD

cd $WORKDIR || fatal "Failed to return to work dir $WORKDIR"

# Установите true для сборки не в hasher
if true || [ `hostname` != sempron.office.etersoft.ru ]; then
	DESTPKG=$RPMDIR/RPMS/$DEFAULTARCH/
	rpmbb $SPECNAME || fatal "Can't build"
else
	rpmbph -LS $SPECNAME || fatal "Can't build"
	DESTPKG=/srv/$USER/hasher-M30/repo/$DEFAULTARCH/
fi
mkdir -p $DESTPKG

DESTDIR=/var/ftp/pvt/Etersoft/Ourside/i586/RPMS.imitator/
build_rpms_name $SPECNAME
if [ -d ${DESTDIR} ] ; then
	rm -f $DESTDIR/$BASENAME*
	echo "Copying to $DESTDIR"
	cp $DESTPKG/$BASENAME-*$VERSION-$RELEASE.* $DESTDIR/ || fatal "Can't copying"
	chmod o+r $DESTDIR/$BASENAME-*
fi

# Для удалённой выкладываем на прежнем месте
if [ -n "$REMOTEBUILD" ] ; then
	DESTDIR=/var/ftp/pvt/Etersoft/Ourside/i586/RPMS.ourside/
	if [ -d ${DESTDIR} ] ; then
		rm -f $DESTDIR/$BASENAME*
		echo "Copying to $DESTDIR"
		cp $DESTPKG/$BASENAME-*$VERSION-$RELEASE.* $DESTDIR/ || fatal "Can't copying"
		chmod o+r $DESTDIR/$BASENAME-*
	fi
fi

if [ -z "$LOCALBUILD" ] ; then
	export EMAIL="$USER@office.etersoft.ru"
	CURDATE=`date`
	RPMFILE=`ls -1 $DESTDIR/$BASENAME-*$VERSION-$RELEASE.* | head -n 1`
	MAILTO="imitator@office.etersoft.ru"
	test -n "$REMOTEBUILD" && MAILTO="$MAILTO kuznetsov@l-start.ru"
	# FIXME: проверка отправки
	mutt $MAILTO -s "[imitator] New build imitator: $VERSION-$RELEASE" <<EOF
Готова новая сборка: $BUILDNAME
Изменения:
`rpm --lastchange -p $RPMFILE`
-- 
your $0
$CURDATE
EOF
	echo "inform mail sent to $MAILTO"
fi

# Увеличиваем релиз и запоминаем спек после успешной сборки
BASERELEASE=$(get_release $SPECNAME | sed -e "s/eter//")
set_release $SPECNAME eter$((BASERELEASE+1))

if [ -z "$LOCALBUILD" ] ; then
	cvs commit -m "Auto updated by $0 for $BUILDNAME" $SPECNAME || fatal "Can't commit spec"
	# Note: we in topscrdir
	TAG=${BUILDNAME//./_}
	echo "Set tag $TAG ..."
	cvs tag $TAG || fatal "Can't set build tag"
fi
echo DONE
date
