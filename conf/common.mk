all:
	for d in ${SUBDIRS}; do (cd $$d && ${MAKE} $@) || exit; done;

dynamic:
	for d in ${SUBDIRS}; do (cd $$d && ${MAKE} $@) || exit; done;

depend:
	for d in ${SUBDIRS}; do (cd $$d && ${MAKE} $@) || exit; done;

clean:
	for d in ${SUBDIRS}; do (cd $$d && ${MAKE} $@); done;

clean_exe:
	for d in ${SUBDIRS}; do (cd $$d && ${MAKE} $@); done;

install:
	for d in ${SUBDIRS}; do (cd $$d && ${MAKE} $@) || exit; done;

re: clean depend all
	
