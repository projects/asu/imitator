#!/bin/sh
#
# Общие функции для используемые в скриптах 
# семейства ctl-xxx
# -----------------------------------------------
# $Id: ctl-functions.sh.in,v 1.1 2007/05/28 21:59:01 pv Exp $
# -----------------------------------------------

PROJECT=@PACKAGE@

prefix=@prefix@
exec_prefix=@exec_prefix@
bindir=@bindir@
datadir=@datadir@
sysconfdir=@sysconfdir@

NO_MYSYSLOG="True"

if [ -n "$RUNLOCAL" ]; then
	localbindir=@top_srcdir@
	. ${localbindir}/conf/bin/ctl-functions.sh

	DEBUG=1
	BINDIR=${localbindir}
	DATADIR=~/tmp/${PROJECT}/share
	CONFDIR=~/tmp/${PROJECT}/etc
	LOGDIR=~/tmp/${PROJECT}/log
	RUNDIR=~/tmp/${PROJECT}/run
	LOCKDIR=~/tmp/${PROJECT}/lock
	BACKUPDIR=~/tmp/${PROJECT}/backup
	VARDIR=~/tmp/${PROJECT}

	. /usr/bin/uniset-functions.sh	
	USERID=
	get_userid
	OMNIPORT=$(expr $USERID + $BASEOMNIPORT)

else
	BINDIR=${bindir}
	DATADIR=${datadir}/@PACKAGE@
	CONFDIR=${sysconfdir}/@PACKAGE@
	VARDIR=/var/local
	LOGDIR=${VARDIR}/log/@PACKAGE@
	RUNDIR=${VARDIR}/run/@PACKAGE@
	LOCKDIR=${VARDIR}/lock
	BACKUPDIR=/var/tmp
	OMNIPORT=2809
fi

CONFILE=$CONFDIR/imitator.xml
MYSYSLOG=$LOGDIR/${PROJECT}_sys.log
DaysAgo=5
#admin=@PACKAGE@_admin
admin=ctl-admin.sh

success()
{
	echo "$PROGNAME: $1 [ OK ]"
	mylog "$PROGNAME: $1 [ OK ]"
}

failure()
{
	echo "$PROGNAME: $1 [ FAILURE ]"
	mylog "$PROGNAME: $1 [ FAILURE ]"
}


mylog()
{
	if [ -n "$NO_MYSYSLOG" ]; then return 0; fi

	tm=$( date )
	if [ -a "$MYSYSLOG" ]; then
		echo "$tm: $1" >> $MYSYSLOG
	else
		echo "$tm: $1" > $MYSYSLOG
	fi
}

msg()
{
	if [ -n "$DEBUG" ]; then echo "$PROGNAME: $1"; fi
	mylog "$PROGNAME: $1"
}

# Check if $pid (could be plural) are running
checkpid()
{
	[ -z "$1" ] && return 1
	
	while [ -n "$1" ]; do
		[ -d "/proc/$1" ] && shift || return 1
	done

	return 0
}

create_pidfile()
{
	pid=$1
	if [ -z "$1" ]; then
		pid=$!
	fi

	echo $pid >$PIDFILE && return 0 || return 1
}

check_status()
{
	PID=
	if [ -a "$PIDFILE" ]; then
		PID=$(cat $PIDFILE)
	fi

	[ -z "$PID" ] && echo "$PROGNAME is stopped" && return 0

	checkpid $PID && echo "$PROGNAME is running" && return 1
	
	echo "$PROGNAME is stopped"
	return 0
}

clear_old_files()
{
	msg "(clear_old_files): begin...(старее $DaysAgo дней)"
	DestDir=$1
	Name=$2
	
	[ -z "$DestDir" ] && [ -z "$Name" ] && msg "(clear_old_files): Не заданы необходимые параметры" && return 1;

	not_exist_dir $DestDir && msg "(clear_old_files): каталога $DestDir не существует " && return 1

	if [ -n "$3" ]; then
		DaysAgo=$3
	fi

#	Удаление БЕЗ записи в логи
	msg "(clear_old_files): find and remove $DestDir -name '*$Name*' -type f -and -mtime +$DaysAgo"
	RESLOG=$( $BINDIR/find $DestDir -name *$Name* -type f -and -mtime +$DaysAgo -print0 | $BINDIR/xargs -r0 /bin/rm -rf )
	if [ -n "$RESLOG" ]; then 
		msg "(clear_old_files): $RESLOG"
	fi

	[ "$RETVAL" -eq 0 ] && success "(clear_old_files): finish" || failure "(clear_old_files): finish"
	return $RETVAL
}

my_pause()
{
	echo -n "pause $1: " 
	for i in `seq 1 $1`
	do
		echo -n "."
		sleep 1
	done
	echo " "
}

check_omni_running()
{
	ret=$( $BINDIR/ctl-omninames.sh status | grep "is running" | wc -l )
	[ $ret \> 0 ] && return 0 || return 1
}

omni_checking()
{
	cat ${CONFILE} | grep -e 'LocalIOR name="1"' 1>/dev/null && return 1 || return 0
}


not_exist_dir()
{
	[ -a "$1" ] && return 1 || return 0
}

start_prog()
{
	if [ -f $CONFDIR/NodeConf.inc ] ; then
		. $CONFDIR/NodeConf.inc
	fi
	$BINDIR/$* $ORBPARAM $NODEPARAM
}


# Ищем подходящее устройство, устанавливая переменные MNTDIR и DEVFLASH, монтируем
mount_flash()
{
	MNTDIR=/mnt/flash
	umount $MNTDIR
	for i in a1 b1 c1 d1 e1 f1 a b c d e f ; do
		DEVFLASH=/dev/sd$i
		#dd if=$DEVFLASH of=/dev/zero count=1 2>/dev/null >/dev/null &&
		mount $DEVFLASH $MNTDIR && return 0
	done
	return 1
}

warning()
{
	echo $@
}


fatal()
{
	umount $MNTDIR
	echo $@
	exit 1
}
