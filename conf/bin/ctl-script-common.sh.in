#!/bin/sh
#
# Общая часть для скриптов запуска/остановки процессов
# -----------------------------------------------
# $Id: ctl-script-common.sh.in,v 1.1 2007/05/28 21:59:01 pv Exp $
# -----------------------------------------------

# DEBUG=1

usage()
{
	echo "${0##*/} {start|stop|restart|condstop|condrestart|status} {-f|--foreground}"
}

if [ -z "$PROGNAME" ] || [ -z "$CMD" ]; then
	usage
	exit 1
fi

# Source function library.
if [ -n "$RUNLOCAL" ]; then
	localbindir=@top_srcdir@
	. ${localbindir}/conf/bin/ctl-functions.sh
else
	prefix=@prefix@
	exec_prefix=@exec_prefix@
	bindir=@bindir@
	. ${bindir}/ctl-functions.sh
fi


# local settings
if [ -z "$PIDFILE" ]; then 
	PIDFILE=$RUNDIR/$PROGNAME.pid
else
	PIDFILE=$RUNDIR/$PIDFILE
fi

if [ -z "$LOCKFILE" ]; then 	
	LOCKFILE=$LOCKDIR/$PROGNAME
else
	LOCKFILE=$LOCKDIR/$LOCKFILE
fi

if [ -f $CONFDIR/ORB.inc ] ; then
	. $CONFDIR/ORB.inc
fi


confparam=
echo ${EXTPARAM} | grep -iws '\-\-confile' || confparam="--confile $CONFILE"


# в foreground-е может быть только запуск!!!!!
if [ "$FOREGROUND" -eq 1 ]; then
	echo "run $PROGNAME in foreground..."
	$BINDIR/$PROGNAME ${confparam} ${EXTPARAM} ${ORBPARAM} ${NODEPARAM}
	exit 0;
fi

RETVAL=0

start()
{
	msg "start"
	
	if [ -z "${NO_CHECK_OMNI}" ] && omni_checking ; then 
		RETVAL=$?
		if [ $RETVAL \> 0 ]; then
			failure "(start): omninames is not running!"
			return 1
		fi
	fi

	PID=
	if [ -a "$PIDFILE" ]; then
		PID=$(cat $PIDFILE)
		
		if [ -n "$PID" ]; then 
			#	проверка может уже запущен	
			checkpid $PID && success "startup" && touch $LOCKFILE && return 0

			msg "(start): check pid from $PIDFILE failed... (remove old pidfile)"
			rm -f $LOCKFILE 
			rm -f $PIDFILE
		fi
	fi
	
	RETVAL=0

	if [ -a $BINDIR/$PROGNAME ]; then
		$BINDIR/$PROGNAME ${confparam} ${EXTPARAM} ${ORBPARAM} ${NODEPARAM} 2>/dev/null 1>/dev/null &
		RETVAL=$?
		PID=$!

		if [ $RETVAL -eq 0 ]; then 
			sleep 1
			checkpid $PID && create_pidfile $PID || RETVAL=1
		fi
	else
		msg "Not found $BINDIR/$PROGNAME"
		RETVAL=1
	fi
	
	[ $RETVAL -eq 0 ] && success "startup" && touch $LOCKFILE || failure "start"
	return $RETVAL
}

stop()
{
	msg "stop"

	PID=
	if [ -a "$PIDFILE" ]; then
		PID=$(cat $PIDFILE)
	fi
	
	[ -z "$PID" ] && failure "stop (unknown pid)" && return 1

	RETVAL=0	
	checkpid $PID && kill $PID && RETVAL=$?

	[ $RETVAL -eq 0 ] && success "stopped" && rm -f $LOCKFILE && rm -f $PIDFILE || failure "stop"
	return $RETVAL
}

restart()
{
	stop
	sleep 1
	start
}

status()
{
	check_status
}

# See how we were called.
case $CMD in
	start)
		start
		;;
	stop)
		stop
		;;
	restart)
		restart
		;;
	condstop)
		if [ -e "$LOCKFILE" ]; then
			stop
		fi
		;;
	condrestart)
		if [ -e "$LOCKFILE" ]; then
			restart
		fi
		;;
	status)
		status
		RETVAL=$?
		;;

	-h|--help|help)
		usage		
		;;
	*)
		usage
		RETVAL=1
esac

exit $RETVAL
