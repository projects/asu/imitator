/***************************************************************************
 * This file is part of the Imitator Project
 *
 * Copyright (C) 2005 Etersoft. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 ****************************************************************************/

/*! \file
 * \brief Определение функций форматирования данных
 *  \author Vitaly Lipatov <lav@etersoft.ru>
 *  \date   $Date: 2005/08/01 10:55:58 $
 *  \version $Id: TypeFormats.h,v 1.7 2005/08/01 10:55:58 lav Exp $
 *
 *
 **************************************************************************/

#ifndef TypeFormats_H_
#define TypeFormats_H_

#include <sstream>
#include <string>
/*!
 * Функция возвращает строку, в которой число f занимает w целых знаков и p знаков дробной части
 */
std::string precise_float(double f,int w, int p)
{
	std::ostringstream os;
	os.width(w+p+(p?1:0));
	os.fill('0');
	os.setf(std::ios_base::fixed);
	os.precision(p);
	if (f >= 0)
	{
		os << f;
		return os.str();
	}
	else
	{
		os << -f;
		return string("-")+os.str();
	}
}

/*!
 * То же, что и precise_float, но указывает знак +,- всегда
 */
std::string precise_signed_float(double f,int w, int p)
{
	return string(f >= 0?"+":"")+precise_float(f,w,p);
}


#endif
