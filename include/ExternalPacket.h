/***************************************************************************
* This file is part of the Imitator Project                                *
* Copyright (C) 2004 Etersoft. All rights reserved.   	                   *
***************************************************************************/
/*! \file
 * \brief Определение пакетов для обмена с внешними устройствами
 *  \author Vitaly Lipatov <lav@etersoft.ru>
 *  \date   $Date: 2007/05/28 21:59:01 $
 *  \version $Id: ExternalPacket.h,v 1.7 2007/05/28 21:59:01 pv Exp $
 *
 *
 * Здесь указываются структуры данных, которыми идёт обмен с внешними устройствами
 *
 **************************************************************************/

#ifndef ExternalPacket_H_
#define ExternalPacket_H_
//----------------------------------------------------------------------------------------------------------------
#include "ImitatorTypes.h"
#include "DataTypes.h"
//----------------------------------------------------------------------------------------------------------------
#include <glibmm.h>
//----------------------------------------------------------------------------------------------------------------

namespace Imitator
{

	/*!
	 * Структура сообщения Сигма-40
	 * \note Многобайтные слова заданы в порядке убывание значимости байт
	 * \note Отрицательные числа заданы в дополнительном коде
	 * \note В положительных числах старший бит всегда равен нулю
	 * Данные упакованы, именно в этом порядке байты передаются в порт
	 * \todo Нужно вставить assert (sizeof(Sigma40Packet)-4 = s40m.numdata)
	 */
	struct Sigma40Packet
	{
		/* 1 Header */
		guchar header1;	//!< Заголовочный байт 5Ah
		guchar header2;	//!< Заголовочный байт A5h
		guchar numdata;	//!< Количество байт данных в сообщении (48h) от ident до data-measurement
		guchar ident;		//!< Идентификатор данных 02h
		/* 5 Status */
		guchar status1;
		guchar status2;
		guchar bite_status;
		/* 8 Data and Time */
		beuint16 date;		//!< Дата * (1 << 15) / 1
		beuint24 time;	//!< Время (<83886 секунд)
		beint16  spare;
		/* 15 Attitude */
		beuint16 heading;	/*!< Курс */
		beint16  roll;		/*!< Крен */
		beint16  pitch;		/*!< Дифферент */
		/* 21 Attitude rate */
		beint16 heading_rate;	/*!< Скорость изменения курса */
		beint16 roll_rate;	/*!< Скорость изменения крена */
		beint16 pitch_rate;	/*!< Скорость изменения дифферента */
		/* 27 INS Position */
		beint32 INS_latitude;	/*!< Широта */
		beint32 INS_longitude;	/*!< Долгота */
		beint16  INS_depth;	/*!< Глубина?? */
		/* 37 INS Position accuracy */	/* Точность?? */
		beuint32 INS_latitude_accuracy;
		beuint32 INS_longitude_accuracy;
		beint16  INS_position_correlation;
		/* 47 GPS Position */
		beint32 latitude;	/*!< Широта */
		beint32 longitude;	/*!< Долгота */
		/* 55 INS Velocity */	/*!< Скорость */
		beint16 north_velocity;	
		beint16 east_velocity;
		beint16 down_velocity;
		/* 61 LOG Velocity */
		beint16 log_speed;
		/* 63 Navigation data */
		beuint16 course_made_good;
		beuint16 speed_over_ground;
		beuint16 set;
		beuint16 drift;
		/* 71 Provision for deflection data measurement */
		guchar data_measurment[6];
		guchar checkbyte;	/*!< Контрольная сумма (складываются байты данных, начиная с numdata)*/
		guchar terminator; 	/*!< Завершающий байт AAh */
	}__attribute__((packed));

	inline bool operator==( Sigma40Packet& ls, Sigma40Packet& rs )
	{
		return !memcmp(&ls,&rs,sizeof(Sigma40Packet));
	}


	// ---------------------------------------------------------------------

	/*! Структура сообщения РДЛ-3
	 * Только для передачи процессу вывода, упаковка данных не требуется
	 */
	struct RDL3Packet
	{
		long Vx; 		/*!< Продольная составляющая скорости */
		long Vy;		/*!< Поперечная составляющая скорости */
		bool check; 	/*!< Контроль */
		bool denied;	/*!< Отказ */
	};


}; // end of Imitator namespace


#endif
