/***************************************************************************
 * This file is part of the Imitator Project
 *
 * Copyright (C) 2004 Etersoft. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 ****************************************************************************/

/*! \file
 * \brief Определение дополнительных типов данных
 *  \author Vitaly Lipatov <lav@etersoft.ru>
 *  \date   $Date: 2005/02/09 11:06:09 $
 *  \version $Id: DataTypes.h,v 1.3 2005/02/09 11:06:09 lav Exp $
 *
 *
 * Опеределяются типы данных для работы с 3-х и 2-х байтными словами
 * записанных в порядке Big Endian
 * \example beuint24 ub; int a; ub = 100; a = ub
 *
 **************************************************************************/

#ifndef DataTypes_H_
#define DataTypes_H_

/*!
 * Новый тип данных - состоит из четырёх байт, старший идёт первым
 * Знаковый
 */
class beint32
{
	unsigned char byte[4];
	public:
		beint32& operator = (int word)
		{
			byte[3] = word & 0xFF;
			byte[2] = (word >>8 ) & 0xFF;
			byte[1] = (word >>16 ) & 0xFF;
			byte[0] = (word >>24 ) & 0xFF;
			return *this;
		}
		operator int ()
		{
			return (byte[0] << 24) | (byte[1] << 16) | (byte[2] << 8) | byte[3];
		}
} __attribute__((packed));


/*!
 * Новый тип данных - состоит из четырёх байт, старший идёт первым
 * Беззнаковый
 */
class beuint32
{
	unsigned char byte[4];
	public:
		beuint32& operator = (unsigned int word)
		{
			byte[3] = word & 0xFF;
			byte[2] = (word >>8 ) & 0xFF;
			byte[1] = (word >>16 ) & 0xFF;
			byte[0] = (word >>24 ) & 0xFF;
			return *this;
		}
		operator unsigned int ()
		{
			return (byte[0] << 24) | (byte[1] << 16) | (byte[2] << 8) | byte[3];
		}
} __attribute__((packed));


/*!
 * Новый тип данных - состоит из трёх байт, старший идёт первым
 * Беззнаковый
 * \todo Возможно нужно сменить функции на friend
 * \todo Нужно посмотреть на G_BYTE_ORDER в devhelp (GDK)
 */
class beuint24
{
	unsigned char byte[3];
	public:
		beuint24& operator = (unsigned int word)
		{
			byte[2] = word & 0xFF;
			byte[1] = (word >>8 ) & 0xFF;
			byte[0] = (word >>16 ) & 0xFF;
			return *this;
		}
		operator unsigned int ()
		{
			return (byte[0] << 16) | (byte[1] << 8) | byte[2];
		}
} __attribute__((packed));


/*!
 * Новый тип данных - состоит из двух байт, старший идёт первым
 * 
 * Беззнаковый
 */
class beuint16
{
	protected:
		unsigned char byte[2];
	public:
		beuint16& operator= (unsigned short word)
		{
			byte[1] = word & 0xFF;
			byte[0] = (word >>8 ) & 0xFF;
			return *this;
		}
		operator unsigned short ()
		{
			return (byte[0] << 8) | byte[1];
		}
} __attribute__((packed));

/*!
 * Новый тип данных - состоит из двух байт, старший идёт первым
 * Знаковый
 */
class beint16
{
	protected:
		unsigned char byte[2];
	public:
		beint16& operator= (short word)
		{
			byte[1] = word & 0xFF;
			byte[0] = (word >>8 ) & 0xFF;
			return *this;
			//return DUBYTE::operator=(word);
		}
		operator short ()
		{
			//return (short)DUBYTE::operator unsigned short;
			return (byte[0] << 8) | byte[1];
		}
} __attribute__((packed));


#endif
