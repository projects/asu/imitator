/***************************************************************************
* This file is part of the Imitator Project                                *
* Copyright (C) 2004 Etersoft. All rights reserved.   	                   *
***************************************************************************/
/*! \file
 * \brief Новые и переопределённые типы данных
 *  \author Anton Korbin <ahtoh@etersoft.ru>
 *  \date   $Date: 2005/01/28 20:52:57 $
 *  \version $Id: ImitatorTypes.h,v 1.24 2005/01/28 20:52:57 pv Exp $
 *
 *
 * Сюда вписываются уникальные для проекта типы данных, константы и т.п.
 */
/**************************************************************************/

#ifndef ImitatorTypes_H_
#define ImitatorTypes_H_
//----------------------------------------------------------------------------------------------------------------
#include <MessageType.h>
#include <ComPort.h>
#include <Debug.h>
//----------------------------------------------------------------------------------------------------------------		
//#include <glibmm.h>
//----------------------------------------------------------------------------------------------------------------
//using namespace std;
//----------------------------------------------------------------------------------------------------------------		

namespace Imitator
{
	extern DebugStream ideb; /*< лог */

	// ---------------------------------------------------------------------
	// Messages
	class ImitatorMessage: 
			public UniSetTypes::Message
	{
		public:
			enum TypeOfMessage
			{
				AnyMessage=UniSetTypes::Message::TheLastFieldOfTypeOfMessage+1, 
				CommMsg,	/*!< Передаются данные сообщения */
				ConfMsg		/*!< Передаётся команда настройки */
			};

			/* Тип сообщения (устройства) */
			enum PacketType
			{
				Sigma40 = 1,
				RDL3,
				GPS,
				GGA, GLL, RMC, VTG, HDT, HDTE, VBW
			};
	};
	
	// -----------------------			
	class CommMessage:
		public ImitatorMessage
	{
		public:
		
			char data[100];	/*!< данные */
			unsigned short size;		/*!< фактический размер данных */
			//PacketType packetType;		/*!< тип принятых данных */

			// конструкторы	
			CommMessage()
			{
				type = ImitatorMessage::CommMsg;
			}

			CommMessage( char* _data, int size, UniSetTypes::ObjectId supplier = UniSetTypes::DefaultObjectId ):
			size(size)
			{
				type = ImitatorMessage::CommMsg;
				this->supplier = supplier;
				assert( size<=sizeof(data) );
				memset(data,0,sizeof(data));
				memcpy(data,_data,size);
			}

			CommMessage( const string _data ):
			size(size)
			{
				type = ImitatorMessage::CommMsg;
				int dsize(_data.size());
				assert( dsize<=sizeof(data) );
				memset(data,0,sizeof(data));
				memcpy(data,_data.c_str(),dsize);
			}

			// преобразования	 
		 	CommMessage(const UniSetTypes::VoidMessage *msg)
			{
				memset(this,0,sizeof(*this));
				memcpy(this,msg,sizeof(*this));
				assert(type == ImitatorMessage::CommMsg);
			}
			
			inline UniSetTypes::TransportMessage transport_msg() const
			{
				return transport(*this);
			}
	};

	// -----------------------			
	class ConfMessage:
		public ImitatorMessage
	{
		public:
			/*! Параметры настройки порта
                            \todo Lav: Параметры настройки порта по идее должны быть стандартной структурой*/
			ComPort::Speed speed;	/*!< скорость порта 300...115200 */
			ComPort::Parity parity;	/*!< наличие контроля чётности */
			ComPort::CharacterSize chr_size;	/*!< размер байта данных */
			ComPort::StopBits stop_bits;	/*!< количество стоп-бит */
			char device[50]; /*!< название устройства (текстовая строка) */
			int timeout;

			/*! Параметры устройства и режим передачи */
			int period;	/*!< период повторения сообщения в мс */
			
			PacketType packetType;	/*!< Тип отправляемых данных */

			/*! Направление передачи */
			enum Direction
			{
				Receive,
				Send
			} direction;

			/*! Команды управления процессом */
			enum Command
			{
				Start,	/*!< Запустить отправку */
				Stop,	/*!< Остановить отправку */
				Config	/*!< Установить параметры */
			} cmd;
			
			/*! Режим работы процесса обмена */
			enum WorkType
			{
				Disabled = 0,
				Enabled,	/*!< Включить реальный обмен */
				Emulated	/*!< Включить эмуляцию */
			} workType;


			// конструкторы	
			ConfMessage()
			{
				type = ImitatorMessage::ConfMsg;
			}

			ConfMessage(Command cmd):
				cmd(cmd)
			{
				type = ImitatorMessage::ConfMsg;
			}
			
			void ConfigComPort(	string _device,
							ComPort::Speed speed,
							ComPort::Parity parity,
							ComPort::CharacterSize chr_size,
							ComPort::StopBits stop_bits,
							int period,
							int timeout)
			{
				memset(&device, 0, sizeof(device));
				// Чтобы быть уверенным в наличии нуля в конце device
				assert(_device.size() < sizeof(device));
				memcpy(&device, _device.c_str(), _device.size());
				this->speed = speed;
				this->parity = parity;
				this->chr_size = chr_size;
				this->stop_bits = stop_bits;
				this->period = period;
				this->timeout = timeout;
			}

			void ConfigLptPort(	string _device, int period )
			{
				memset(&device, 0, sizeof(device));
				assert(_device.size() < sizeof(device));
				memcpy(&device, _device.c_str(), _device.size());
				this->period = period;
			}

			// преобразования	 
		 	ConfMessage(const UniSetTypes::VoidMessage *msg)
			{
				memcpy(this,msg,sizeof(*this));
				assert(type == ImitatorMessage::ConfMsg);
			}
			
			inline UniSetTypes::TransportMessage transport_msg() const
			{
				return transport(*this);
			}
	};

}; // end of Imitator namespace


#endif
