#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
using namespace std;

#include "DataTypes.h"
#include "TypeFormats.h"


#define DATETYPETEST(type, typelocal, value, str) \
{ \
	type db; \
	typelocal sh=value; \
	printf ("Testing %s ... for %x\n",__STRING(type), sh); \
	typelocal tt; \
	char *t; \
	db = sh; \
	t = (char*) &db; \
	assert (sh == db); \
	tt = db; \
	assert (tt == sh); \
	assert (!strncmp(t,str,strlen(str))); \
}

int main(int argc, char** argv)
{

//	Тесты для новых типов данных

	DATETYPETEST(beint32, int, -5, "\xFF\xFF\xFF\xFB");
	DATETYPETEST(beuint32, unsigned int, 0xA1A2A3A4, "\xA1\xA2\xA3\xA4");
	DATETYPETEST(beuint24, unsigned int, 0xA1A2A3, "\xA1\xA2\xA3");
	DATETYPETEST(beuint16, unsigned short, 0xA1A2, "\xA1\xA2");
	DATETYPETEST(beint16, short, -5, "\xFF\xFB");

	printf(" All tests PASSED\n");

// Перевод градусов в числовой формат
	printf("\nПроверка конвертирования градусов(float) --> градусы минуты\n");
	float val=23.123123;

	int deg = (int)val;
	int min = (int)((val-deg)*60*10000);
	min /=10000;
	
	printf("Исходное значение: %f\nРезультат: deg=%d min=%d\n",val,deg,min);

// Проверка вывода символов
	cout << endl << "Тестирование precise_float" << endl;
	string s="013", sr=precise_float(13, 3, 0);
	cout << "Должно быть " << s << ": " << sr << endl;
	assert ( s == sr);
	s="0013.235000" ; sr=precise_float(13.235, 4, 6);
	cout << "Должно быть " << s << ": " << sr << endl;
	assert ( s == sr);
	s="-013.250"; sr=precise_float(-13.25, 3, 3);
	cout << "Должно быть " << s << ": " << sr << endl;
	assert ( s == sr);
	s="000.000"; sr=precise_float(0.0, 3, 3);
	cout << "Должно быть " << s << ": " << sr << endl;
	assert ( s == sr);

	cout << endl << "Тестирование precise_signed_float" << endl;
	s="-013.250"; sr=precise_signed_float(-13.25, 3, 3);
	cout << "Должно быть " << s << ": " << sr << endl;
	assert ( s == sr);
	s="+013.250"; sr=precise_signed_float(13.25, 3, 3);
	cout << "Должно быть " << s << ": " << sr << endl;
	assert ( s == sr);
	s="+000.000"; sr=precise_signed_float(0.0, 3, 3);
	cout << "Должно быть " << s << ": " << sr << endl;
	assert ( s == sr);

	return 0;
}
