#include <sstream>
#include <Configuration.h>
#include <ObjectsActivator.h>
#include <Debug.h>
#include "RDL3Server.h"
#include "ImitatorTypes.h"

using namespace UniSetTypes;
using namespace std;

static void short_usage()
{
	cout << "Usage: rdl3-server --name ObjectId [--confile configure.xml] [--dev filename ]\n";
}

DebugStream Imitator::ideb;

int main(int argc,char** argv)
{

	try
	{
		if( argc>1 )
		{
			if( !strcmp(argv[1],"--help") )
			{
				short_usage();
				return 0;
			}
			else if( !strcmp(argv[1],"-v") )
			{
				cout << "rdl3-server: $Id: main.cc,v 1.6 2007/05/28 21:59:02 pv Exp $ " << endl;
				return 0;
			}
		}

		string confile( UniSetTypes::getArgParam("--confile",argc,argv) );
		if( confile.empty() )
			confile = string(IMITATOR_CONFDIR) + "imitator.xml";

		string dev( UniSetTypes::getArgParam("--dev",argc,argv) );
		if( dev.empty() )
			dev = "/dev/lpt0";

		conf = new Configuration(argc, argv, confile);

		// Настройка логов
		ostringstream logname;
		string dir(conf->getLogDir());
//		string pname(argv[0]);
		logname << dir << "rdl3-server.log";

		Imitator::ideb.logFile( logname.str().c_str() );
		unideb.logFile( logname.str().c_str() );
		conf->initDebug((DebugStream&)Imitator::ideb,"ImitatorDebug");

		// определяем ID объекта
		ObjectId ID = DefaultObjectId;
		string name = conf->getArgParam("--name");
		if( !name.empty() )
		{
			ID = conf->getObjectID(name);	
			if( ID == UniSetTypes::DefaultObjectId )
			{
				cerr << "(main): идентификатор '" << name 
					<< "' не найден в конф. файле!"
					<< " в секции " << conf->getObjectsSection() << endl;
				return 0;
			}
		}

		RDL3Server em(ID,dev);
		ObjectsActivator act;
		act.addObject(static_cast<class UniSetObject*>(&em));

		ideb[Debug::INFO] << " ------------ START --------------- \n";

		act.run(true);
		em.execute();
	}
	catch(Exception& ex)
	{
		cerr << "(main): " << ex << endl;
	}
	catch(...)
	{
		cerr << "(main): catch ..." << endl;
	}

	return 0;
}
