#ifndef RDL3Server_H_
#define RDL3Server_H_

#include <string>
#include <queue>
#include <stdarg.h>

#include <UniSetObject_LT.h>
#include <PassiveTimer.h>
#include "ImitatorTypes.h"
#include "../Emulator/BitPortRDL3.h"

using namespace Imitator;

class RDL3Server:
	public UniSetObject_LT
{
	public:
		RDL3Server( UniSetTypes::ObjectId id, const string dev="/dev/lpt0" );
		virtual ~RDL3Server();
		void execute();

	protected:
		// Функции обработки пришедших сообщений 
		virtual void processingMessage( UniSetTypes::VoidMessage *msg );
		void processingConfMessage(ConfMessage& sm);
		void send();

	private:
		PassiveTimer ptSend;
		CommMessage sendData;
		UniSetTypes::ObjectId guiID;

		/*! Текущий режим работы */
		ConfMessage::WorkType work;
		/*! Тип устройств передачи и приёма */
		ConfMessage::PacketType typeSend;

		int curSleepTime;

		string dev; /*!< LPT устройство */
		BitPortRDL3* bp;
};

#endif // RDL3Server_H_
