#include <queue>
#include <Configuration.h>
#include "ImitatorTypes.h"
#include "RDL3Server.h"

// --------------------------------------------------------------------------------
using namespace UniSetTypes;
using namespace Imitator;
using namespace std;
// --------------------------------------------------------------------------------


RDL3Server::RDL3Server( ObjectId id, const string dev ):
UniSetObject_LT(id),
curSleepTime(UniSetTimer::WaitUpTime),
dev(dev),
bp(0)
{
	thread(false);
	guiID = conf->getObjectID("GUI");
	work = ConfMessage::Disabled;
	
	if( dev != "/dev/null" )
		bp = new BitPortRDL3();
	else
	  	ideb[Debug::INFO]  << myname << "(init): RUN IN IMITATOR MODE (dev=/dev/null) " << endl;
}

// --------------------------------------------------------------------------------

RDL3Server::~RDL3Server()
{
	delete bp;
}

void RDL3Server::send()
{
/*
	if( work == ConfMessage::Emulated )
	{
		sendEmulated();
		return;
	}
*/
//	assert(2-2);
}

void RDL3Server::execute()
{
	while(1)
	{
		if( waitMessage(msg, curSleepTime) )
			processingMessage(&msg);
		
		if( work == ConfMessage::Enabled )
		{
//			if (ptSend.checkTime())
//			{
				if( bp )
					bp->send((RDL3Packet*)&sendData.data);
//				ptSend.reset();
//			}
		}
		else
			curSleepTime = UniSetTimer::WaitUpTime;
			
		// msleep(100);
	}
}

void RDL3Server::processingConfMessage(ConfMessage& cfm)
{
	//assert (cfm.supplier == guiID );
	ideb[Debug::INFO]  << myname << "(processingConfMessage): " << cfm.cmd << endl;
	switch (cfm.cmd)
	{
		case ConfMessage::Start:
			//work = cfm.workType;
			work = ConfMessage::Enabled;
			curSleepTime = 2000;
			//assert (cfm.workType == ConfMessage::Enabled ||
			//	cfm.workType == ConfMessage::Emulated);
			break;

		case ConfMessage::Stop:
			work = ConfMessage::Disabled;
			curSleepTime = UniSetTimer::WaitUpTime;
			break;

		case ConfMessage::Config:
			// Не обрабатываем
			break;
			assert (work);
			if (cfm.direction == ConfMessage::Send)
			{
				ptSend.setTiming(cfm.period);
				typeSend == cfm.packetType;
				assert (cfm.packetType == ConfMessage::RDL3);
				//curSleepTime = c
			}
			else
				assert(1-1);
			//break;

		default:
			assert(0);
	}
}

// --------------------------------------------------------------------------------
void RDL3Server::processingMessage( UniSetTypes::VoidMessage *msg )
{
	try
	{
		switch( msg->type )
		{
			case ImitatorMessage::CommMsg:
			{
				CommMessage sm( msg );
				//assert (sm.supplier == guiID );
				// Во время работы можно присылать данные только того же размера

				ideb[Debug::INFO]  << myname << "(processingMessage): " << sm.size << "  " << "RDL3Packet size:" << sizeof(RDL3Packet) << endl;
				//assert (work || sm.size == sendData.size);
				sendData = sm;
				work = ConfMessage::Enabled;
				curSleepTime = 2000;
			}
			break;

			case ImitatorMessage::ConfMsg:
			{
				ConfMessage cm(msg);
				processingConfMessage(cm);
			}
			break;

			default:
				break;
		}	
	}
	catch(Exception& ex)
	{
//		ideb[Debug::CRIT]  << myname << "(processingMessage): " << ex << endl;
	}
}
