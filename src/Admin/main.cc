#include <sstream>
#include <iomanip>
#include <iostream>
#include <Configuration.h>
#include <UniversalInterface.h>
#include <ComPort.h>
#include "ImitatorTypes.h"

using namespace UniSetTypes;
using namespace Imitator;
using namespace std;

static void print_help(int width, const string cmd, const string help, const string tab=" " )
{
	// Создаём свой stream,
	// чтобы не менять параметры основного потока
	ostringstream info;
	info.setf(ios::left, ios::adjustfield);
	info << tab << setw(width) << cmd << " - " << help;
	cout << info.str();
}

static void short_usage()
{
	cout << "Usage: admin --command [params] [--name ObjectID] [--confile imitator.xml]\n";
	cout << "Параметры:\n--------\n";
	print_help(20,"--name","ID процесса обмена.\n");
	cout << "\nКоманды:\n-------------\n";
	print_help(20,"--start ","Начать обмен. \n");
	print_help(20,"--stop","Остановить обмен.\n");
	print_help(20,"--data DataString","Данные для процесса обмена.\n");
	cout << "\n";
	print_help(20,"--config-comport [params] [--name ObjectID]","Настройка COM-порта.\n");
	cout << "параметры для настройки COM-порта:\n";
	print_help(20,"-d device","Устройство. По умолчанию /dev/ttyS1\n");
	print_help(20,"-s speed","Скорость передачи. По умолчанию 9600.\n");
	print_help(20,"-cs character_size","По умолчанию CSize5\n");
	print_help(20,"-sb stop_bits","По умолчанию OneBit\n");
	print_help(20,"-t timeout","По умолчанию 1000 msec\n");
	print_help(20,"--parity parity","Чётность. По умолчанию NoParity.\n");
	print_help(20,"--period period","По умолчанию 100 msec\n");

}

static void log_error(ostream& os, const string msg )
{
	os << "(admin): " << msg << endl;
}

static UniSetTypes::ObjectId getServer( const string defaultServer="CommServer")
{
	string oname(conf->getArgParam("--name"));		
	if( oname.empty() )
		oname = defaultServer;
				
	// получаем полное имя
	oname = conf->getObjectsSection()+"/"+oname;

	UniSetTypes::ObjectId oid = conf->oind->getIdByName(oname);
	if( oid == UniSetTypes::DefaultObjectId )
	{
		log_error(cerr,"(start): НЕ НАЙДЕН идентификатор объекта "+ oname);
		exit(1);
	}
	
	return oid;
}


DebugStream Imitator::ideb;

int main(int argc, char** argv)
{
	try
	{
		if( argc>1 && !strcmp(argv[1],"--help") )
		{
			short_usage();
			return 0;
		}

		if( argc < 2 )
		{
			log_error(cout, "НЕ ЗАДАНО КОМАНД");
			return 0;
		}

		string confile( UniSetTypes::getArgParam("--confile",argc,argv) );
		if( confile.empty() )
			confile = string(IMITATOR_CONFDIR) + "imitator.xml";

		conf = new Configuration(argc, argv, confile);
		UniversalInterface ui;
		
		// Processing
		const char* cmd = argv[1]+2;  
		if( !strcmp(cmd, "start") || !strcmp(cmd, "stop") )
		{
			UniSetTypes::ObjectId oid = getServer();

			ConfMessage msg;
			if( !strcmp( cmd, "start") )
				msg.cmd = ConfMessage::Start;
			else
				msg.cmd = ConfMessage::Stop;

			TransportMessage tmsg(msg.transport_msg());
			ui.send(oid,tmsg);
		}
		else if( !strcmp(cmd, "data") )
		{
			string data = conf->getArgParam("--data");
			if( data.empty() )
			{
				log_error(cerr, "НЕ УКАЗАНА СТРОКА С ДАННЫМИ");
				return 1;
			}

//			if( data.size() > sizeof(ConfMessage::data) )
//			{
//				ostreamstring msg;
//				msg <<  "СЛИШКОМ ДЛИННАЯ СТРОКА ДАННЫХ ( > " << sizeof(ConfMessage::data) << ")";
//				log_error(cerr,msg.str());
//				return 1;
//			}

			UniSetTypes::ObjectId oid = getServer();
			CommMessage	msg(data);
			TransportMessage tmsg(msg.transport_msg());
			ui.send(oid,tmsg);
		}
		else if( !strcmp(cmd, "config-comport") )
		{
			UniSetTypes::ObjectId oid = getServer();
		
			string dev	 	= conf->getArgParam("-d","/dev/ttyS1");
			int period		= conf->getArgInt("--period","100" );
			int timeout		= conf->getArgInt("-t","1000" );
		
			// speed
			ComPort::Speed speed(ComPort::ComSpeed9600);
			string tmp = conf->getArgParam("-s","9600");
			if( tmp == "2400" )
				speed = ComPort::ComSpeed2400;
			if( tmp == "4800" )
				speed = ComPort::ComSpeed4800;
			if( tmp == "9600" )
				speed = ComPort::ComSpeed9600;
			else if( tmp == "19200" )
				speed = ComPort::ComSpeed19200;
			else
			{
				log_error(cerr, "(config-comport): Задано некорректное значение скорости передачи (-s)"); 
				return 1;
			}

			// parity
			ComPort::Parity parity;
			tmp = conf->getArgParam("--parity","NoParity");
			if( tmp == "NoParity" )
				parity = ComPort::NoParity;
			else if( tmp == "Odd" )
				parity = ComPort::Odd;
			else if( tmp == "Mark" )
				parity = ComPort::Mark;
			else if( tmp == "Space" )
				parity = ComPort::Space;
			else if( tmp == "Even" )
				parity = ComPort::Even;
			else
			{
				log_error(cerr, "(config-comport): Задано некорректное значение parity (-parity)");
				return 1;
			}
			
			// character size
			ComPort::CharacterSize csize;
			tmp = conf->getArgParam("-cs","CSize5");
			if( tmp == "CSize5" )
				csize = ComPort::CSize5;
			else if( tmp == "CSize6" )
				csize = ComPort::CSize6;
			else if( tmp == "CSize7" )
				csize = ComPort::CSize7;
			else if( tmp == "CSize8" )
				csize = ComPort::CSize8;
			else
			{
				log_error(cerr, "(config-comport): Задано некорректное значение character_size (-cs)");
				return 1;
			}
			
			// stop_bits 
			ComPort::StopBits stop_bits;
			tmp = conf->getArgParam("-sb","OneBit");
			if( tmp == "OneBit" )
				stop_bits = ComPort::OneBit;
			else if( tmp == "OneAndHalfBits" )
				stop_bits = ComPort::OneAndHalfBits;
			else if( tmp == "TwoBits" )
				stop_bits = ComPort::TwoBits;
			else
			{
				log_error(cerr, "(config-comport): Задано некорректное значение stop_bits (-sb)"); 
				return 1;
			}

			ConfMessage msg;
			msg.ConfigComPort(dev,speed,parity,csize,stop_bits,period,timeout);
			TransportMessage tmsg(msg.transport_msg());
			ui.send(oid,tmsg);
		}
		else
		{
			log_error(cerr, "неизвестная команда \n");
		}		
	}
	catch(Exception& ex)
	{
		cerr << "(admin): " << ex << endl;
	}
	catch(...)
	{
		cerr << "(main): catch ..." << endl;
	}

	return 0;
}

// -----------------------------------
