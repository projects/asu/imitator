#include <Configuration.h>
#include "CommServer.h"
#include "ExternalPacket.h"
// --------------------------------------------------------------------------------
using namespace UniSetTypes;
using namespace Imitator;
using namespace std;
// --------------------------------------------------------------------------------

CommServer::CommServer( ObjectId id, string mode ):
	UniSetObject_LT(id),
	port(0),
	mode(mode),
	stop(true)
{
	guiid = conf->oind->getIdByName(conf->getObjectsSection() + "/GUIMessenger");
	if(id == DefaultObjectId)
		ideb[Debug::CRIT]<< myname << " Object GUIMessenger not found" << endl;
	if(mode != "s" && mode != "r")
	{
		ideb[Debug::CRIT] << myname << " Don't know mode (s - send or r - receive) ...exit" << endl;
		throw Exception();
	}

	if(mode == "s")
	{
		thread = new ThreadCreator<CommServer>(this, &CommServer::send);
	}
	else if(mode == "r")
	{
		thread = new ThreadCreator<CommServer>(this, &CommServer::receive);
	}				
}

// --------------------------------------------------------------------------------

CommServer::~CommServer()
{
}

// --------------------------------------------------------------------------------
/*
void CommServer::callback()
{
	try
	{
		if( receiveMessage(msg) )
			processingMessage(&msg);
	}
	catch(...){}
}
*/
// --------------------------------------------------------------------------------

void CommServer::processingMessage( UniSetTypes::VoidMessage *msg )
{
	try
	{
		ideb[Debug::INFO] << myname << " processingMessage " << endl;
		switch( msg->type )
		{
/*
			case Message::SensorInfo:
			{
				SensorMessage sm( msg );
				sensorInfo( &sm );
				break;
			}
*/
			case Message::SysCommand:
			{
				SystemMessage sm( msg );
				sysCommand( &sm );
				break;
			}
/*
			case Message::Timer:
			{
				TimerMessage tm(msg);
				timerInfo(&tm);
				break;
			}
*/
			case CommMessage::CommMsg:
			{
				ideb[Debug::INFO] << myname << " COMM " << endl;
				if(!port)
					break;
				CommMessage sm( msg );
				memset(&cmsg, 0, sizeof(cmsg));
				memcpy(&cmsg.data, &sm.data, sm.size);
				cmsg.size = sm.size;

				if(stop)
				{
					stop = false;
					thread->start();
				}
				break;
			}

			case ConfMessage::ConfMsg:
			{
				ConfMessage cfmsg( msg );
				if(cfmsg.cmd == ConfMessage::Start)
				{
					ideb[Debug::INFO] << myname << " START " << endl;
					if(mode == "r")
					{
						stop = false;
						thread->start();
					}
				}
				else if(cfmsg.cmd == ConfMessage::Stop)
				{
					ideb[Debug::INFO] << myname << " STOP " << endl;
					stop = true;
					msleep(500);
					delete thread;
					if(mode == "s")
					{
						thread = new ThreadCreator<CommServer>(this, &CommServer::send);
					}
					else if(mode == "r")
					{
						thread = new ThreadCreator<CommServer>(this, &CommServer::receive);
					}				
				}
				else if(cfmsg.cmd == ConfMessage::Config)
				{
					ideb[Debug::INFO] << myname << " CONFIG " << endl;
					stop = true;
					msleep(500);
					delete thread;
					if(mode == "s")
					{
						thread = new ThreadCreator<CommServer>(this, &CommServer::send);
					}
					else if(mode == "r")
					{
						thread = new ThreadCreator<CommServer>(this, &CommServer::receive);
					}				
					create_comm_device(cfmsg);
				}
				break;
			}

			default:
//				ideb[Debug::WARN] << myname << ": неизвестное сообщение  " << msg->type << endl;
				break;
		}	
	}
	catch(Exception& ex)
	{
//		ideb[Debug::CRIT]  << myname << "(processingMessage): " << ex << endl;
	}
}
// ------------------------------------------------------------------------------------------

void CommServer::sensorInfo( SensorMessage *sm )
{
}

// ------------------------------------------------------------------------------------------

void CommServer::timerInfo( TimerMessage *tm )
{
}

// ------------------------------------------------------------------------------------------

void CommServer::sysCommand( SystemMessage *sm )
{
	switch( sm->command )
	{
		case SystemMessage::StartUp:
		{
//			askSensors(UniversalIO::UIONotify);
			break;
		}				
		
		case SystemMessage::FoldUp:								
		case SystemMessage::Finish:
			askSensors(UniversalIO::UIODontNotify);
			break;
		
		case SystemMessage::WatchDog:
			askSensors(UniversalIO::UIONotify);
			break;
		case SystemMessage::LogRotate:
			{
				string fname = ideb.getLogFile();
				if( !fname.empty() )
				  ideb.logFile(fname.c_str());
			}
			{
				string fname = unideb.getLogFile();
				if( !fname.empty() )
				  unideb.logFile(fname.c_str());
			}
			break;

		default:
			break;
	}
}

// ------------------------------------------------------------------------------------------

void CommServer::askSensors( UniversalIO::UIOCommand cmd )
{
//	try
//	{
//	}
//	catch( Exception& ex )
//	{
//		ideb[Debug::CRIT] << myname << "(askSensors): " << ex << endl;
//	}
}

// ------------------------------------------------------------------------------------------

void CommServer::send()
{
	ideb[Debug::INFO] << myname << " Sending data..." << endl;
	if(cmsg.data[0] == 0x5A && cmsg.data[1] == 0xA5)
	{
/*
		ideb[Debug::INFO] << myname << " Send sigma40 message: size=" << cmsg.size << endl;
		for(int i = 0; i < cmsg.size; i++)
			printf(" 0x%X", cmsg.data[i]);
		printf("\n");
*/
	}
	else if(cmsg.data[0] == '$')
	{
//		ideb[Debug::INFO] << myname << " Send text message: " << cmsg.data << " size=" << cmsg.size << endl;
	}
	else
	{
		ideb[Debug::INFO] << myname << " Unknown message: don't send" << hex << cmsg.data << endl;
		return;
	}	

//	stop = false;
	for(;;)
	{
		if(stop)
			break;
		try
		{
			port->sendBlock(reinterpret_cast<unsigned char*>(cmsg.data), cmsg.size);
			if(cmsg.data[0] == 0x5A && cmsg.data[1] == 0xA5)
			{
				ideb[Debug::INFO] << myname << " Send sigma40 message: size=" << cmsg.size << ", period=" << sleep << "ms" << endl;
				string str;
				for(int i = 0; i < cmsg.size; i++)
				{
					char buf[100];
					sprintf(buf, " 0x%X", cmsg.data[i]);
					string str1(buf);
					str = str + str1;
				}
				ideb[Debug::INFO] << str << endl << endl;
			}
			else if(cmsg.data[0] == '$')
			{
				ideb[Debug::INFO] << myname << " Send text message: size=" << cmsg.size << ", period=" << sleep << "ms" << endl;
				string str(cmsg.data, cmsg.size - 2);
				ideb[Debug::INFO] << str << "<CR><LF>" << endl << endl;
/*
				for(int i = 0; i < cmsg.size; i++)
				{
					char buf[100];
					sprintf(buf, " 0x%X", cmsg.data[i]);
					string str1(buf);
					str = str + str1;
				}
*/
				ideb[Debug::INFO] << str << endl << endl;
			}
			msleep(sleep);
		}
		catch(...)
		{
			ideb[Debug::INFO] << myname << " Send?" << endl;
		}
	}
}

// ------------------------------------------------------------------------------------------

void CommServer::receive()
{
	ideb[Debug::INFO] << myname << " Receiving data..." << endl;

	char data[100];
	for(;;)
	{
		if(stop)
			break;
		try
		{
			memset(data, 0, sizeof(data));
			char byte1 = port->receiveByte();
//			char byte2 = port->receiveByte();
			int i;
			if(byte1 == 0x5A)
//			if(byte1 == 0x5A && byte2 == 0xA5)
			{
				char byte2 = port->receiveByte();
				ideb[Debug::INFO] << myname << " Sigma40 message received..." << endl;
				data[0] = byte1;
				data[1] = byte2;
				int size = sizeof(Sigma40Packet);
				for(i = 2; i < 90; i++)
				{
					data[i] = port->receiveByte();
					if(data[i] == 0xAA)
					{
						ideb[Debug::INFO] << myname << " Received, size=" << i + 1 << ", period=" << sleep << "ms" << endl;
						string str;
						for(int i = 0; i < size; i++)
						{
							char buf[100];
							sprintf(buf, " 0x%X", data[i]);
							string str1(buf);
							str = str + str1;
						}
						ideb[Debug::INFO] << str << endl << endl;
//							ideb[Debug::INFO] << " 0x" << (int)data[i];
//						ideb[Debug::INFO] << endl << endl;
						break;
					}
				}
			}
			else if(byte1 == '$')
			{
				ideb[Debug::INFO] << myname << " Receive text message..." << endl;
				data[0] = byte1;
				char byte2 = port->receiveByte();
				data[1] = byte2;
				bool flag = false;
				for(i = 2; i < 90; i++)
				{
					data[i] = port->receiveByte();
//					printf(" 0x%X", data[i]);
//					if(data[i] == '\n')
//						break;
//				

					if(data[i] == '\r')
					{
						ideb[Debug::INFO] << myname << " Receive CR" << endl;
						flag = true;
					}
					else if(data[i] == '\n' && flag == true)
					{
						ideb[Debug::INFO] << myname << " Receive LF" << endl;
						ideb[Debug::INFO] << myname << " Received, size=" << i + 1 << ", period=" << sleep << "ms" << endl;
						string str(data, i-1);
						ideb[Debug::INFO] << str << "<CR><LF>" << endl << endl;
						break;
					}
					else
						flag = false;
				}
			
				if(i >= 90)
				{
					string str;
					for(int i = 0; i < 90; i++)
					{
						char buf[100];
						sprintf(buf, " 0x%X", data[i]);
						string str1(buf);
						str = str + str1;
					}
					ideb[Debug::INFO] << str << endl << endl;
				}
			}
			else
			{
				ideb[Debug::INFO] << myname << " Unknown message header is received" << endl;
				string str;
				char buf[100];
				sprintf(buf, " 0x%X", byte1);
				string str1(buf);
				str = str + str1;
				ideb[Debug::INFO] << str << endl << endl;
				throw TimeOut();
			}
			if(guiid == DefaultObjectId)
				guiid = conf->oind->getIdByName(conf->getObjectsSection() + "/GUIMessenger");
			if(guiid == DefaultObjectId)
				ideb[Debug::CRIT] << myname << " Object GUIMessenger not found" << endl;
			else
			{
				TransportMessage tm(CommMessage(data, i + 1).transport_msg());
				try
				{
					ui.send(guiid, tm);
				}
				catch( UniSetTypes::Exception& ex )
				{
					ideb[Debug::INFO] << myname << " " << ex << endl;
				}
			}

//			printf("\n");
//			msleep(sleep);
		}
		catch(TimeOut)
		{
			ideb[Debug::INFO] << myname << " Timeout" << endl;
//			msleep(sleep);
		}
	}
}

// ------------------------------------------------------------------------------------------

void CommServer::create_comm_device(ConfMessage &msg)
{
	ideb[Debug::INFO] << myname << " Create comm device " << msg.device << endl;
	if(!port)
		delete port;
	try
	{
		port = new ComPort(msg.device);
		port->setSpeed(msg.speed);
		port->setParity(msg.parity);
		port->setCharacterSize(msg.chr_size);
		port->setStopBits(msg.stop_bits);
		port->setTimeout(msg.timeout);
		port->setWaiting(true);
		sleep = msg.period;
	}
	catch(SystemError &err)
	{
		ideb[Debug::INFO] << myname << "::create_comm_device " << err << endl;
	}
}

// ------------------------------------------------------------------------------------------
