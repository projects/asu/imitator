#ifndef CommServer_H_
#define CommServer_H_

#include <string>
#include <UniSetObject_LT.h>
#include <ComPort.h>
#include <ImitatorTypes.h>
#include <ThreadCreator.h>

class CommServer:
	public UniSetObject_LT
{
	public:
		CommServer( UniSetTypes::ObjectId id, string mode );
		virtual ~CommServer();

	protected:
		virtual void processingMessage( UniSetTypes::VoidMessage *msg );
		void askSensors( UniversalIO::UIOCommand cmd );
		void sysCommand( UniSetTypes::SystemMessage *sm );
		void sensorInfo( UniSetTypes::SensorMessage *sm );
		void timerInfo( UniSetTypes::TimerMessage *tm );
		void receive();
		void send();
		void create_comm_device(Imitator::ConfMessage &msg);
//		virtual void callback();
	
	private:
		ComPort *port;
		bool stop;
		bool speed;
		unsigned int sleep;
		string mode;
		Imitator::CommMessage cmsg;
		UniSetTypes::ObjectId guiid;
//		UniSetTypes::uniset_mutex channelMutex[2];
		ThreadCreator<CommServer> *thread;
};

#endif // CommServer_H_
