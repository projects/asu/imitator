/***************************************************************************
 * This file is part of the Imitator Project
 *
 * Copyright (C) 2005 Etersoft. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 ****************************************************************************/

/*! \file
 * \brief Интерфейс РДЛ-3
 *  \author Vitaly Lipatov <lav@etersoft.ru>
 *  \date   $Date: 2007/05/28 21:59:02 $
 *  \version $Id: BitPortRDL3.cc,v 1.5 2007/05/28 21:59:02 pv Exp $
 *
 * См. работу с портом (parport) в COMEDI
 * file:/usr/share/doc/HOWTO/HTML/en/IO-Port-Programming/IO-Port-Programming.html
 * COMEDI
 *
 **************************************************************************/

#include <sched.h>
#include <sys/mman.h>
#include <time.h>

//#include <asm/system.h>
#define local_irq_disable() 	__asm__ __volatile__("cli": : :"memory")
#define local_irq_enable()	__asm__ __volatile__("sti": : :"memory")

#include "BitPortRDL3.h"

using namespace std;


// Отправка кодового сообщения
// Длительность - 175 = 17*(5+5)+5 мкс
void BitPortRDL3::send(int codeVx, int codeVy)
{
	mlockall(MCL_CURRENT); // Блокируем всю используемую память процесса
	local_irq_disable(); // Disable IRQ??
	// \note Отрицательные значения передаются в прямом коде??
	// Преобразуем из обратного и добавляем знаковый бит в старший разряд
	if (codeVx<0)
		codeVx=(-codeVx) | (1<<(NumberOfMSB));
	if (codeVy<0)
		codeVy=(-codeVy) | (1<<(NumberOfMSB));

	set(strobe);
	// Выдаём коды младшим разрядом вперёд, i - номер бита
	for (int i=0;i<=NumberOfMSB;i++)
	{
		// Vx,Vy ... tick^5 ... tick_5
		//cout << i << endl;
		if (codeVx & (1<<i)) set(Vx); else unset(Vx);
		if (codeVy & (1<<i)) set(Vy); else unset(Vy);
		put_wait(5);

		set(tick);
		put_wait(5);

		unset(tick);
	}
	unset(strobe);
	unset(Vx);
	unset(Vy);
	wait(5);
	put();

	local_irq_enable(); // Enable IRQ??
	munlockall(); // Разблокируем память
}

// Отправка сообщения согласно пакету
void BitPortRDL3::send(Imitator::RDL3Packet *bp)
{
	
	if (bp->check)
		set(check);
	else
		unset(check);

	if (bp->denied)
		set(denied);
	else
		unset(denied);

	send(bp->Vx, bp->Vy);
	Imitator::ideb[Debug::INFO] << "check:" << bp->check << " denied:" << bp->denied << "  Vx=" << bp->Vx << " Vy=" << bp->Vy << endl;

}

#ifdef RDL_DEBUG
int main()
{
	// Устанавливать real-time только на время отправки

	// see the manual page for sched_setscheduler(2) for details
	// Memory  locking is usually needed for real-time processes to avoid pag-
	// ing delays, this can be done with mlock or mlockall.
	// Tune Kernel Scheduler
	
	BitPortRDL3 bp;
	for (char b=0;;b^=0xff)
	{
//		io.out(0x378,b);
//		cout << "RDL step\n";
		struct sched_param sp;
		/* Не работает на ядрах 2.6
		// Устанавливаем наивысший приоритет
		sp.sched_priority=sched_get_priority_max(SCHED_FIFO);
		if (sched_setscheduler(0, SCHED_FIFO,&sp))
		{
			cout << sp.sched_priority << " failed\n";
			return 1;
		}
		*/
		bp.send(3200,1920);
		/*
		// Возвращаемся к обычному расписанию
		sp.sched_priority=0;
		if (sched_setscheduler(0, SCHED_OTHER,&sp))
		{
			cout << "0 failed\n";
			return 1;
		}
		*/
		usleep(50);
	}
	return 0;
}
#endif
