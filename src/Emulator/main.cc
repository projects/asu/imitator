#include <sstream>
#include <Configuration.h>
#include <ObjectsActivator.h>
#include <Debug.h>
#include "Emulator.h"
#include "ImitatorTypes.h"

using namespace UniSetTypes;
using namespace std;

static void short_usage()
{
	cout << "Usage: send-server --name ObjectId [--confile configure.xml]\n";
}

DebugStream Imitator::ideb;

#ifndef RDL_DEBUG

int main(int argc,char** argv)
{

	try
	{
		if( argc>1 && !strcmp(argv[1],"--help") )
		{
			short_usage();
			return 0;
		}

		string confile( UniSetTypes::getArgParam("--confile",argc,argv) );
		if( confile.empty() )
			confile = string(IMITATOR_CONFDIR) + "imitator.xml";

		conf = new Configuration(argc, argv, confile);

		// Настройка логов
		ostringstream logname;
		string dir(conf->getLogDir());
		string pname(argv[0]);
		logname << dir << pname << ".log";
		Imitator::ideb.logFile( logname.str().c_str() );
		unideb.logFile( logname.str().c_str() );
		conf->initDebug((DebugStream&)Imitator::ideb,"ImitatorDebug");
		// ---

		// определяем ID объекта
		ObjectId ID(DefaultObjectId);
		string name = conf->getArgParam("--name");
		if( !name.empty() )
		{
			ID = conf->oind->getIdByName(conf->getObjectsSection()+"/"+name);	
			if( ID == UniSetTypes::DefaultObjectId )
			{
				cerr << "(main): идентификатор '" << name 
					<< "' не найден в конф. файле!"
					<< " в секции " << conf->getObjectsSection() << endl;
				return 0;
			}
		}

		Emulator em(ID);
		ObjectsActivator act;
		act.addObject(static_cast<class UniSetObject*>(&em));
		act.run(true);
		em.execute();
	}
	catch(Exception& ex)
	{
		cerr << "(main): " << ex << endl;
	}
	catch(...)
	{
		cerr << "(main): catch ..." << endl;
	}

	return 0;
}

#endif
