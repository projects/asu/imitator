/***************************************************************************
 * This file is part of the Imitator Project
 *
 * Copyright (C) 2005 Etersoft. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 ****************************************************************************/

/*! \file
 * \brief Интерфейс РДЛ-3
 *  \author Vitaly Lipatov <lav@etersoft.ru>
 *  \date   $Date: 2005/08/01 10:43:33 $
 *  \version $Id: BitPortRDL3.h,v 1.3 2005/08/01 10:43:33 lav Exp $
 *
 * См. работу с портом (parport) в COMEDI
 * file:/usr/share/doc/HOWTO/HTML/en/IO-Port-Programming/IO-Port-Programming.html
 * COMEDI
 *
 **************************************************************************/


#include <sched.h>
#include <sys/mman.h>
#include <time.h>
#include <sys/io.h>

#include "IOAccessOld.h"
#include "ExternalPacket.h"

#ifndef BitPortRDL3_H_
#define BitPortRDL3_H_

class BitPort: IOAccess
{
	public:
		BitPort(): IOAccess(0x378, 7)
		{
			new IOAccess(0x80, 1);
			address=0x378;
			byte=0;
			put();
			// open port
		}

	protected:
		inline void set(int bit)
		{
			byte|=bit;
		}
	
		inline void unset(int bit)
		{
			byte&=~bit;
		}
	
		inline void put()
		{
			out(address,byte);

		}
		inline void put_wait(int n)
		{
			put();
			// Получается, 2мкс уходит на вывод в порт
			for (int i=0;i<n-2;i++)
				in(0x80);
		}
		// wait - ожидание в мкс
		inline void wait(int n)
		{
		//	nanosleep(); // Недостаточная точность и нужно сохранение структуры
		// Основано на том, что чтение из порта задерживает на 1мкс
		//sleep(1);
			for (int i=0;i<n;i++)
			{
				in(0x80);
//				put();
//				for (int b=0;b<5;b++)
//					int a=1;
			}
		
		}

	private:
		unsigned int byte;
		unsigned int address;
};

class BitPortRDL3: BitPort
{
	public:
		enum
		{			// B Cont
			tick	= 1,	// 0 2 - тактовый импульс
			strobe	= 2,	// 1 3 - стробирующий импульс
			Vx	= 4,	// 2 4 - код продольной составл. скорости
			Vy	= 8,	// 3 5 - код поперечной составл. скорости
			check	=16,	// 4 6 - служебный сигнал "Контроль"
			denied	=32	// 5 7 - служебный сигнал "Отказ"
					//  18 - GND
		};

		BitPortRDL3(): BitPort()
		{
			// NumberOfMSB = 16;
			//set(run);
			//unset(busy);
			//put();
		}

		void send(int codeVx, int codeVy);
		void send(Imitator::RDL3Packet *bp);

	protected:
		// Номер старшего разряда кода
		//const int NumberOfMSB = 16;
		#define NumberOfMSB 16
};

#endif
