#ifndef ReceivedData_H_
#define ReceivedData_H_

#include <queue>

// --------------------------------------------------------------------------------
using namespace UniSetTypes;
using namespace Imitator;
using namespace std;
// --------------------------------------------------------------------------------

class ReceivedData : public std::queue<char>
{
	public:
		ReceivedData()
		{
			data = NULL;
			beginMark = false;
			//finished = false;
			endMark = false;
			error = false;
		}

		bool processing();

		// Добавить данные к буферу
		void addData(const char *data, int size)
		{
			for (int i=0; i<size; i++)
				push(data[i]);
		}
		char * getData();
	private:
		bool beginMark, endMark, error;
		void checkMark(const string mark);
		string BeginString, EndString;
		char *data;
};

#endif
