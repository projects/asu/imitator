#ifndef Emulator_H_
#define Emulator_H_

#include <string>
#include <queue>
#include <stdarg.h>

#include <UniSetObject_LT.h>
#include <PassiveTimer.h>

#include "ImitatorTypes.h"
#include "ReceivedData.h"

using namespace Imitator;


// \todo поменять на queue - empty, push('A'), char a=pop(), clear
class EmulatedData : public std::queue<unsigned char>
{
	public:
		EmulatedData()
		{
			isFinished = false;
			beginMark = false;
			endMark = false;
		}

		char * getData() { return NULL;  } // pop

		// Добавить данные к буферу
		void addData(const char *data, int size)
		{
			for (int i=0; i<size; i++)
				push(data[i]);
		}
		void add(const string str)
		{
			addData(str.c_str(),str.size());
		}
		EmulatedData& operator<< (const string str)
		{
			add(str);
			return *this;
		}

		void addf(const char *fmt, ...)
		{
			/* Guess we need no more than 100 bytes. */
			char p[100];
			va_list ap;

			va_start(ap, fmt);
			vsnprintf (p, 100, fmt, ap);
			va_end(ap);
			addData(p,strlen(p));
		}
		// Начать заполнение заново
		void rewind() { isFinished = false; }
		// Установить признак готовности - НЕ должна использоваться!
		void finish() { isFinished = true; }
		// Буфер готов к отправке
		bool finished() { return true || isFinished; }
	private:
		bool isFinished;
		bool beginMark, endMark;
	//	const int MaxDataSize;
};

class Emulator:
	public UniSetObject_LT
{
	public:
		Emulator( UniSetTypes::ObjectId id );
		virtual ~Emulator();
		void execute();

	protected:
		// Функции обработки пришедших сообщений 
		virtual void processingMessage( UniSetTypes::VoidMessage *msg );
		void processingConfMessage(ConfMessage sm);
		void send();
		void sendEmulated();
		void receive();

	private:
		PassiveTimer ptReceive, ptSend;
		CommMessage sendData;
		EmulatedData emulatedData;
		ReceivedData receivedData;
		UniSetTypes::ObjectId guiID;
		/* Текущий режим работы */
		ConfMessage::WorkType work;
		/* Тип устройств передачи и приёма */
		ConfMessage::PacketType typeSend;
		ConfMessage::PacketType typeReceive;
		/* Для эмуляции */
		void putLatitude (float latitude);
		void putLongitude (float longitude);

};

#endif // Emulator_H_
