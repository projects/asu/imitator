
#include "ImitatorTypes.h"
#include "ExternalPacket.h"
#include "Emulator.h"
#include "TypeFormats.h"


// Вынести их в отдельный файл для Антона
/*! Выводит Широту в виде строки 
 * LLLL.LLLL,S/N
 * \params latit - градусы
*/

/*
void Emulator::putLatitude(float latit)
{
	bool negative = (latit < 0);
	if (negative)
		latit = -latit;
	int deg = (int)latit;
	float min = ((latit-deg)*60.);
	assert( deg <= 90 );
	emulatedData.addf("%02d%02.04f,",deg,min);
	emulatedData.push( negative ? 'N' : 'S');
}
*/

/*! Выводит Долготу в виде строки
 *  YYYYY.YYYY,E/W
 * \params longi - градусы
*/
/*
void Emulator::putLongitude(float longi)
{
	bool negative = (longi < 0);
	if (negative)
		longi = -longi;
	int deg = (int)longi;
	float min = ((longi-deg)*60.);
	assert( deg <= 180 );
	emulatedData.addf("%03d%02.04f,",deg, min);
	emulatedData.push( negative ? 'W' : 'E');
}
*/

void Emulator::sendEmulated()
{
// контрольную сумму и знак
	if (typeSend == ConfMessage::RDL3 && typeReceive == ConfMessage::VBW)
	{
		RDL3Packet pt;
		assert(sizeof(RDL3Packet) == sendData.size);
		memcpy(&pt, sendData.data, sizeof(RDL3Packet));

		float Vx = (float)pt.Vx / 100;
		float Vy = (float)pt.Vy / 100;
		
		// По ТЗ должны быть нулевыми ??
		//if (!pt.check && !pt.denied &&
		if ( 
			Vx <= 60.0 && Vx >= 0.0 &&
			Vy <= 25.0 && Vy >= -25.0)
		{
			emulatedData << "$VDVBW," << precise_float(Vx,2,2) << "," <<
				precise_signed_float(Vy,2,2) <<",a,,,*hh\r\n";
		}
		else
			assert(2-2);
	}

/*	else if (typeSend == ConfMessage::GPS && typeReceive == ConfMessage::GPS)
	{
		emulatedData.addData(sendData.data,sendData.size);
	}
*/
	else if (typeSend == ConfMessage::Sigma40)
	{
		Sigma40Packet pt;

		assert(sizeof(Sigma40Packet) == sendData.size);
		memcpy(&pt, sendData.data, sizeof(Sigma40Packet));

		// Общие пересчёты
		//float latitude = pt.INS_latitude	* 90. / (1 << 15);
		//float longitude = pt.INS_longitude	* 180. / (1 << 15);

		switch(typeReceive)
		{
			case ConfMessage::HDT:
			case ConfMessage::HDTE:
			{
				// \fixme Не пересчитаны! Что со знаком? Что за u?
				float	heading = pt.heading	* 360. / (1 << 16),
					roll	= pt.roll	* 90. / (1 << 15),
					pitch	= pt.pitch	* 90. / (1 << 15),
					heading_rate  = pt.heading_rate * (1 << 15) / 1;
				if (typeReceive == ConfMessage::HDTE)
					emulatedData << "$HEHDTE," << precise_float(heading,3,2)
						<< ",T," << precise_signed_float(roll,2,2)
						<< ","	<< precise_signed_float(pitch,2,2)
						<< ","	<< precise_signed_float(heading_rate,1,3)
						<< "*hh\r\n";
				else
					emulatedData << "$HEHDT," << precise_float(heading,3,2)
						<< ",T*hh\r\n";
				break;
			}
/*			
			case ConfMessage::GGA:
				emulatedData << "$GPGGA,000000,";
				putLatitude(latitude);
				emulatedData.push(',');
				putLongitude(longitude);
				emulatedData << ",1,2,00.00,,M,,M,,0000*hh\n\r";
				break;

			case ConfMessage::GLL:
				emulatedData << "$GPGLL";
				putLatitude(latitude);
				emulatedData.push(',');
				putLongitude(longitude);
				emulatedData << ",000000,A,*hh\n\r";
				break;

			case ConfMessage::RMC:
				emulatedData << "$GPGLL";
				putLatitude(latitude);
				emulatedData.push(',');
				putLongitude(longitude);
				emulatedData << ",12.34,056.78,010104,,*hh\n\r";
				break;

			case ConfMessage::VTG:
				emulatedData << "$GPVTG,123.45,T,000.0,M,06.78,N,00.00,K*hh\n\r";
				break;

*/			default:
				ideb << "Получено команда принять неизвестное сообщение " << typeReceive;
				
		}
	}
	else
	{	// Сообщения, которые не знаем, просто прогоняем
		//ideb << "Получено команда отправить сообщение " << typeSend <<
		//	" и принять сообщение " << typeReceive;
		emulatedData.addData(sendData.data,sendData.size);
		assert (typeSend);
	}
}

