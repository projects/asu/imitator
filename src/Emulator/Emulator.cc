#include <queue>

#include <Configuration.h>

#include "ImitatorTypes.h"
#include "Emulator.h"
#include "BitPortRDL3.h"

// --------------------------------------------------------------------------------
using namespace UniSetTypes;
using namespace Imitator;
using namespace std;
// --------------------------------------------------------------------------------


Emulator::Emulator( ObjectId id ):
UniSetObject_LT(id)
{
	thread(false);
	guiID = conf->oind->getIdByName(conf->getObjectsSection() + "/GUI");
	work = ConfMessage::Disabled;
}

// --------------------------------------------------------------------------------

Emulator::~Emulator()
{
}

// Принимаем из порта, что есть
// Эмуляцию на уровне объекта, из которого байты вынимаются
// Объединять объекты приёма/передачи при необходимости
// Обрабатывать таймаут - и при эмуляции
// Не застревать в функции приёма
void Emulator::receive()
{

	if (work != ConfMessage::Emulated)
	{
		// Настоящий приём
		receivedData.addData("",0);
	}

	if (receivedData.processing())
	{
		CommMessage msg(receivedData.getData(),receivedData.size());
		// указывать с какого порта принято?
		// указывать флаг ошибки при необходимости
		TransportMessage tmsg( msg.transport_msg() );
		ui.send(guiID, tmsg);
	}
}


void Emulator::send()
{
	if (work == ConfMessage::Emulated)
	{
		sendEmulated();
		return;
	}
	else
	{
		// отправляем
		//BitPortRDL3 bp;
	}

	assert(2-2);
}

void Emulator::execute()
{
	while(1)
	{
		waitMessage(msg, 100);
		if (work)
		{
			if (ptReceive.checkTime())
			{
				receive();
				ptReceive.reset();
			}
			if (ptSend.checkTime())
			{
				send();
				ptSend.reset();
			}
		}
		// msleep(100);
	}
}

void Emulator::processingConfMessage(ConfMessage cfm)
{
	assert (cfm.supplier == guiID );
	switch (cfm.cmd)
	{
		case ConfMessage::Start:
			work = cfm.workType;
			assert (cfm.workType == ConfMessage::Enabled ||
				cfm.workType == ConfMessage::Emulated);
			break;
		case ConfMessage::Stop:
			work = ConfMessage::Disabled;
			break;
		case ConfMessage::Config:
			assert (work);
			// Должно присылаться для какого типа интерфейса
			// приём/передача
			if (cfm.direction == ConfMessage::Receive)
			{
				ptReceive.setTiming(cfm.period);
				assert (cfm.packetType == ConfMessage::RDL3);
				typeReceive == cfm.packetType;
			}
			else if (cfm.direction == ConfMessage::Send)
			{
				ptSend.setTiming(cfm.period);
				typeSend == cfm.packetType;
			}
			else
				assert(1-1);
			break;
		default:
			assert(0);
	}
}

// --------------------------------------------------------------------------------
void Emulator::processingMessage( UniSetTypes::VoidMessage *msg )
{
	try
	{
		switch( msg->type )
		{
			case ImitatorMessage::CommMsg:
			{
				CommMessage sm( msg );
				assert (sm.supplier == guiID );
				// Во время работы можно присылать данные только того же размера
				assert (work && sm.size == sendData.size);
				sendData = sm;
				break;
			}
			case ImitatorMessage::ConfMsg:
				processingConfMessage( ConfMessage ( msg ) );
				break;

			default:
				break;
		}	
	}
	catch(Exception& ex)
	{
//		ideb[Debug::CRIT]  << myname << "(processingMessage): " << ex << endl;
	}
}
