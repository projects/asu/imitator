/***************************************************************************
* This file is part of the Imitator Project                                *
* Copyright (C) 2004 Etersoft. All rights reserved.   	                   *
***************************************************************************/
/*! \file
 *  \author Pavel Vainerman <pv@etersoft.ru>
 *  \date   $Date: 2005/05/03 15:51:06 $
 *  \version $Id: Devices.h,v 1.6 2005/05/03 15:51:06 pv Exp $
 */
/**************************************************************************/

#ifndef Devices_H_
#define Devices_H_
//--------------------------------------------------------------------------
#include <list>
#include <string>
//--------------------------------------------------------------------------
/*
	// ПРИМЕР ИСПОЛЬЗОВАНИЯ
	list<SerialInfo> lst = get_serial_devices();
	for( list<SerialInfo>::const_iterator it=lst.begin();it!=lst.end();++it)
		cout << it->name << ": " << it->dev << endl;
*/

namespace Imitator
{
	struct SerialInfo
	{
		std::string name;	/*!< название устройства */
		std::string dev; 	/*!< ссылка на устройство  /dev/xxx */
	};
	
	/*! получение списка последовательных устройств */
	std::list<SerialInfo> get_serial_devices();

	// вспомогательные функции
	bool read_usbserial( std::list<SerialInfo>& lst, const std::string dirname="/sys/bus/usb-serial/devices" );
	void read_serial( std::list<SerialInfo>& lst, const std::string dirname="/sys/class/tty" );
	bool dir_exist( const std::string dirname );
	

}; // end of Imitator namespace

#endif
