/***************************************************************************
* Copyright (C) 2005 Etersoft All rights reserved.  					   *
***************************************************************************/
#ifndef Messenger_H
#define Messenger_H

#include <gtkmm.h>
#include <glibmm.h>
#include <UniSetObject.h>
#include <ImitatorTypes.h>

class Messenger: public UniSetObject,
					public Gtk::Frame
{   
public:
    Messenger( UniSetTypes::ObjectId id, string title );
    ~Messenger();
	virtual void processingMessage( UniSetTypes::VoidMessage *msg );
	typedef Gtk::TreeModel::Children type_children;

protected:

  //Tree model columns:
	class ModelColumns1 : public Gtk::TreeModel::ColumnRecord
	{
		public:

		ModelColumns1()
    	{ add(col_time); add(col_message); add(col_check); add(col_fg_color); add(col_bg_color); }

		Gtk::TreeModelColumn<string> col_time;
		Gtk::TreeModelColumn<string> col_message;
		Gtk::TreeModelColumn<string> col_check;
		Gtk::TreeModelColumn<string> col_fg_color;
		Gtk::TreeModelColumn<string> col_bg_color;
	};

	ModelColumns1 Columns1;

	void addMessage(Imitator::CommMessage &cm);

	Glib::RefPtr<Gtk::ListStore> refTreeModel1;

	Gtk::TreeView *treeview1;

	Gtk::TreeModel::Row row;

	int maxmsg;
	int count_rows;

	void startUp();
	void finish();

	Imitator::CommMessage cm_orig;
	Glib::ustring str_orig;

	bool get_message();

private:	
};

#endif
