/***************************************************************************
* Copyright (C) 2005 Etersoft All rights reserved.          			   *
***************************************************************************/
//----------------------------------------------------------------------------------------
#include "Messenger.h"
#include "ImitatorTypes.h"
#include "ExternalPacket.h"
//----------------------------------------------------------------------------------------
using namespace UniSetTypes;
using namespace Imitator;
using namespace std;
//----------------------------------------------------------------------------------------
Messenger::Messenger( UniSetTypes::ObjectId id, string title ):
	UniSetObject(id),
	maxmsg(10),
	count_rows(0)
{
	thread(false);
	Glib::signal_timeout().connect(sigc::mem_fun(*this, &Messenger::get_message), 100);
	
	{
//		string str(conf->getPropByNodeName(string("Messenger"), string("MaxMsg")));
//		maxmsg = atoi(str.c_str());
	}

	//Create the Tree model:
	refTreeModel1 = Gtk::ListStore::create(Columns1);
	treeview1 = manage(new class Gtk::TreeView(refTreeModel1));

	{
		Gtk::CellRendererText *pRenderer = Gtk::manage( new Gtk::CellRendererText() );
		int cols_count = treeview1->append_column(Glib::locale_to_utf8("Время"), *pRenderer);
		Gtk::TreeViewColumn *pColumn = treeview1->get_column(cols_count-1);
		pColumn->set_resizable(false);
		pColumn->set_min_width(80);
		pColumn->add_attribute(pRenderer->property_text(), Columns1.col_time);
		pColumn->add_attribute(pRenderer->property_background(), Columns1.col_bg_color);
		pColumn->add_attribute(pRenderer->property_foreground(), Columns1.col_fg_color);
//		pRenderer->property_foreground().set_value("white");
//		pRenderer->property_font().set_value("Arial Normal 12");
	}

	{
		Gtk::CellRendererText *pRenderer = Gtk::manage( new Gtk::CellRendererText() );
		int cols_count = treeview1->append_column(Glib::locale_to_utf8("Сообщение"), *pRenderer);
		Gtk::TreeViewColumn *pColumn = treeview1->get_column(cols_count-1);
		pColumn->set_resizable();
		pColumn->set_min_width(600);
		pColumn->add_attribute(pRenderer->property_text(), Columns1.col_message);
		pColumn->add_attribute(pRenderer->property_background(), Columns1.col_bg_color);
		pColumn->add_attribute(pRenderer->property_foreground(), Columns1.col_fg_color);
//		pRenderer->property_background().set_value("red");
//		pRenderer->property_foreground().set_value("white");
//		pRenderer->property_font().set_value("Arial Normal 12");
	}
	{
		Gtk::CellRendererText *pRenderer = Gtk::manage( new Gtk::CellRendererText() );
		int cols_count = treeview1->append_column(Glib::locale_to_utf8("Статус"), *pRenderer);
		Gtk::TreeViewColumn *pColumn = treeview1->get_column(cols_count-1);
		pColumn->set_resizable();
		pColumn->set_min_width(100);
		pColumn->add_attribute(pRenderer->property_text(), Columns1.col_check);
		pColumn->add_attribute(pRenderer->property_background(), Columns1.col_bg_color);
		pColumn->add_attribute(pRenderer->property_foreground(), Columns1.col_fg_color);
//		pRenderer->property_background().set_value("red");
//		pRenderer->property_foreground().set_value("white");
//		pRenderer->property_font().set_value("Arial Normal 12");
	}

	Gtk::ScrolledWindow *scrolledwindow1 = manage(new class Gtk::ScrolledWindow());
	scrolledwindow1->set_flags(Gtk::CAN_FOCUS);
	scrolledwindow1->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
	scrolledwindow1->add(*treeview1);
	scrolledwindow1->show();

	this->add(*scrolledwindow1);

	this->set_label(Glib::locale_to_utf8(title));
	this->show_all();

	{
		Glib::RefPtr<Gtk::TreeSelection> refTreeSelection = treeview1->get_selection();
		refTreeSelection->set_mode(Gtk::SELECTION_NONE);
	}
}

//----------------------------------------------------------------------------------------

Messenger::~Messenger()
{   
}

//----------------------------------------------------------------------------------------

void Messenger::addMessage(CommMessage &cm)
{
		char *old=setlocale(LC_NUMERIC, "C");
		if(++count_rows > maxmsg)
		{
			type_children children = refTreeModel1->children();
			type_children::iterator iter = children.begin();
			refTreeModel1->erase(iter);
			count_rows--;
		}

		Gtk::TreeIter iter = refTreeModel1->append();
		row = *(iter);
		treeview1->scroll_to_row(refTreeModel1->get_path(iter));

    	time_t t = cm.tm.tv_sec;    
    	tm tms = *localtime(&t);
		char res_time[100];
		sprintf(res_time,"%02d:%02d:%02d", tms.tm_hour, tms.tm_min, tms.tm_sec);
  		row[Columns1.col_time] = res_time;

		if(cm.data[0] == 0x5A && cm.data[1] == 0xA5)
		{
			char buf[100];
			sprintf(buf, "Sigma40 size=%d, cs=0x%X", cm.size, cm.data[sizeof(Sigma40Packet) - 2]);
	  		row[Columns1.col_message] = buf;
			
			ideb[Debug::INFO] << myname << " memcmp=" << memcmp(&cm.data, &cm_orig.data, sizeof(cm.data)) << endl;
			if(!memcmp(&cm.data, &cm_orig.data, sizeof(Sigma40Packet)))
	  		{
				row[Columns1.col_check] = Glib::locale_to_utf8("Совпадение");
				row[Columns1.col_bg_color] = "green";
			}
			else
	  		{
				row[Columns1.col_check] = Glib::locale_to_utf8("Не совпадение");
				row[Columns1.col_bg_color] = "orange red";
				row[Columns1.col_fg_color] = "white";
			}
			string str;
			for(int i = 0; i < cm.size; i++)
			{
				char buf[100];
				sprintf(buf, " 0x%X", cm.data[i]);
				string str1(buf);
				str = str + str1;
			}
			ideb[Debug::INFO] << str << endl << "Receive" << endl;

			str.clear();
			for(int i = 0; i < cm_orig.size; i++)
			{
				char buf[100];
				sprintf(buf, " 0x%X", cm_orig.data[i]);
				string str1(buf);
				str = str + str1;
			}
			ideb[Debug::INFO] << str << endl << "Orig" << endl;

		}
		else if(cm.data[0] == '$')
		{
	  		string str(cm.data);
	  		string str1(cm.data, cm.size - 2);
			row[Columns1.col_message] = str1 + "<CR><LF>";
			ideb[Debug::INFO] << myname << " str=" << str << "size=" << str.size() << endl;
			ideb[Debug::INFO] << myname << " str_orig=" << str_orig << "size=" << str_orig.size() << endl;
			if(str == str_orig)
	  		{
				row[Columns1.col_check] = Glib::locale_to_utf8("Совпадение");
				row[Columns1.col_bg_color] = "green";
			}
			else
  			{
				row[Columns1.col_check] = Glib::locale_to_utf8("Не совпадение");
				row[Columns1.col_bg_color] = "orange red";
				row[Columns1.col_fg_color] = "white";
			}
		}
		setlocale(LC_NUMERIC, old);
}

//----------------------------------------------------------------------------------------

void Messenger::processingMessage( UniSetTypes::VoidMessage *msg )
{
	switch(msg->type)
	{
		case CommMessage::CommMsg:
		{
			char *old=setlocale(LC_NUMERIC, "C");
			CommMessage cm(msg);
//			ideb[Debug::INFO] << myname << " supplier=" << cm.supplier << endl;
//			ideb[Debug::INFO] << myname << " messenger=" << conf->oind->getIdByName(conf->getObjectsSection() + "/GUIManager") << endl;
			if(cm.supplier == conf->oind->getIdByName(conf->getObjectsSection() + "/GUIManager"))
			{
				if(cm.data[0] == '$')
				{
					#warning "Зачем здесь ustring? Lav (10.07.2005)"
					Glib::ustring str(cm.data, cm.size);
					str_orig.clear();
					str_orig = str;
					ideb[Debug::INFO] << myname << " str_orig=" << str_orig << " size=" << str_orig.size() << endl;
				}
				else if(cm.data[0] == 0x5A && cm.data[1] == 0xA5)
				{
					ideb[Debug::INFO] << myname << " Sigma40 orig, size=" << cm.size << endl;
					memset(&cm_orig, 0, sizeof(cm_orig));
					memcpy(&cm_orig, &cm, sizeof(cm));
//					cm_orig = cm;
				}
			}
			else
			{
				ideb[Debug::INFO] << myname << " Received, size=" << cm.size << endl;
				addMessage(cm);
			}
			setlocale(LC_NUMERIC, old);
			break;
		}
	}
}

//----------------------------------------------------------------------------------------

bool Messenger::get_message()
{
//	cout << "get message Manager" << endl;
//	while(receiveMessage(msg))
	for( int i=0; i<=5; i++ )
	{
		if( receiveMessage(msg) )
			processingMessage(&msg);
		else 
			break;
	}

	return true;	
}
