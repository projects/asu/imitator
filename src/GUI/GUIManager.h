// File:        This file is part of UniSet library
// Copyright:   2003-2004 SET Research Institute. All rights reserved.
// -----------------------------------------------------------------------------------------
#ifndef _GUIManager_H
#define _GUIManager_H
//----------------------------------------------------------------------------------------
/*! \class GUIManager
 * \author Anthony Korbin
 * \date   March 2004
 * \version $Id: GUIManager.h,v 1.7 2007/05/28 21:59:02 pv Exp $
 *
 *	Базовый класс для создания визуального менеджера объектов
 *	\todo Что-нибудь надо сделать
 *	\todo Написать описание создания менеджера для графического интерфейса (на примере)
*/ 

#include <ObjectsManager.h>
#include <libglademm.h>
#include <glibmm.h>
#include <gtkmm.h>
#include "Devices.h"

class GUIManager: 
	public ObjectsManager
{   
public:
    GUIManager( UniSetTypes::ObjectId id, Glib::RefPtr<Gnome::Glade::Xml> &refXml );
    ~GUIManager();
	virtual void processingMessage( UniSetTypes::VoidMessage *msg );

protected:
	void on_toolbutton_send_start_activate();
	void on_toolbutton_send_stop_activate();
	void on_toolbutton_recv_start_activate();
	void on_toolbutton_recv_stop_activate();
//	void on_toolbutton_send_opt_activate();
//	void on_toolbutton_recv_opt_activate();
	void on_combobox_send_changed();
	void on_combobox_recv_changed();
//	void on_button_send_confirm_opt_activate();

private:
	Glib::RefPtr<Gnome::Glade::Xml> refXml;
//	UniSetTypes::ObjectId sserver, rserver, messenger;
	Gtk::ComboBox *combo_send;
	Gtk::ComboBox *combo_recv;
	Gtk::ComboBoxEntryText *combo_send_dev, *combo_recv_dev;
//	Gtk::ComboBoxEntry *combo_s_dev, *combo_s_speed, *combo_s_byte, *combo_s_parity, *combo_s_stopb, *combo_s_period;
	Gtk::Frame *frame_Sigma40;
	Gtk::Frame *frame_GPS;
	Gtk::Frame *frame_RDL3;

	void get_str_latitude(char *buf, double latit);
	void get_str_longtitude(char *buf, double longt);
	void add_gps_check_sum(char *buf);
	int gps_msg_sizeof(char *buf);
	Gtk::Statusbar *sb;
	Gtk::Window *window_send_opt;

	Gtk::Button* button_send_start;
	Gtk::Button* button_send_stop;
	Gtk::Button* button_recv_start;
	Gtk::Button* button_recv_stop;

	std::list<Imitator::SerialInfo> serlist;
	
	void detect_devices();
	
	enum send
	{
		Sigma40_s,
		GPS_s,
		RDL3_s
	};
	enum recv
	{
		Sigma40_r,
		HDT_r,
		HDTE_r,
		VBW_r,
		GPS_r,
		NoCheck_r
	};
	
	UniSetTypes::ObjectId id_gui;
	UniSetTypes::ObjectId id_send;
	UniSetTypes::ObjectId id_recv;
	UniSetTypes::ObjectId id_rdl3;
};

#endif
