/***************************************************************************
* This file is part of the Imitator Project                                *
* Copyright (C) 2004 Etersoft. All rights reserved.   	                   *
***************************************************************************/
/*! \brief Получение списка устройств
 *  \author Pavel Vainerman <pv@etersoft.ru>
 *  \date   $Date: 2007/05/28 21:59:02 $
 *  \version $Id: Devices.cc,v 1.10 2007/05/28 21:59:02 pv Exp $
 */
/**************************************************************************/

#include <string>
#include <list>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/types.h>
#include <errno.h>
#include "Devices.h"
#include "ImitatorTypes.h"


using namespace Imitator;
using namespace std;
//------------------------------------------------------------------------------
bool Imitator::read_usbserial( list<SerialInfo>& lst, const string dirname )
{
	DIR *dir;
	struct dirent* entry;
	dir = opendir (dirname.c_str());
	if( dir == NULL )
	{
    	ideb[Debug::CRIT] << "(read_usbserial): Не смог открыть каталог " << dirname << endl;
		return false;
	}

	while( (entry = readdir(dir)) != NULL )
	{
		if( !strcmp(entry->d_name,".") || !strcmp(entry->d_name,"..") )
			continue;
	
		string fullname(dirname+"/"+entry->d_name);
		char target[255];
		bzero(target,sizeof(target));

		if( readlink(fullname.c_str(),target,sizeof(target)) <= 0 )
		{	
			ideb[Debug::CRIT] << "(read_usbserial): не смог получить информацию по файлу " << entry->d_name << endl; 
			ideb[Debug::CRIT] << "(read_usbserial): " << strerror(errno) << endl; 
			continue;
		}
		
//		Ссылка может существовать, а файла на которую она ссылается может не быть,
//		поэтому надо проверять ещё, что существует файл. Например через fopen...
		string fname(dirname+"/"+target);
		FILE *fp = fopen(fname.c_str(), "r");
		if(!fp)
		{	
			ideb[Debug::CRIT] << "(read_usbserial): Не смогли открыть файл " << target << endl; 
			ideb[Debug::CRIT] << "(read_usbserial): " << strerror(errno) << endl; 
			continue;
		}
		else
			fclose(fp);

		ideb[Debug::INFO] << "(read_usbserial): check link: " << target << endl;

		// по хорошему надо обнулять все массивы кажется bzero...
		char tmp[100]; char pci[100];
		char dev[20]; char num1[20];
		char usb[20]; char usb1[20];
			
		// обнуляем 
		bzero(tmp,sizeof(tmp));
		bzero(pci,sizeof(pci));
		bzero(dev,sizeof(dev));
		bzero(num1,sizeof(num1));
		bzero(usb,sizeof(usb));
		bzero(usb1,sizeof(usb1));
		
		int num_ctrl(0); 	// номер контроллера
		int num_con(0); 	// номер разъёма
		// пример строки
		// ../../../devices/pci0000:00/0000:00:02.0/usb2/2-3/2-3:1.0/ttyUSB0
		int res = sscanf(target,"%[./]devices/%[^/]/%[^/]/%[^/]/%d-%d/%[^/]/%[^/]",
							&tmp,&pci,&num1,&usb,&num_con,&num_ctrl,&usb1,&dev);
		if( res != 0 )
		{
//			cout << "\n****" << num_ctrl << "-" << num_con << " --> " << "/dev/" << dev << endl;
			ostringstream nm;
			nm << "USB" << num_ctrl << "-" << num_con;
			SerialInfo si;
			si.name 	= nm.str();
			si.dev 		= "/dev/"+string(dev);
			lst.push_front(si);

			ideb[Debug::INFO] << "(read_usbserial): add to list " << nm.str() << endl;
		}
		else
			ideb[Debug::CRIT] << "(read_usbserial): SCANF FAILURE for " << target << endl;
    }
	closedir (dir);
}
//------------------------------------------------------------------------------
void Imitator::read_serial( list<SerialInfo>& lst, const string dirname )
{
	DIR *dir;
	struct dirent* entry;
	dir = opendir(dirname.c_str());
	if( dir == NULL )
	{
    	ideb[Debug::CRIT] << "(read_serial): Не смог открыть каталог " << dirname << endl;
		return;
	}

	while( (entry = readdir(dir)) != NULL )
	{
		if( !strcmp(entry->d_name,".") || !strcmp(entry->d_name,"..") )
			continue;

		int num;
		int res = sscanf(entry->d_name,"ttyS%d", &num);
		if( res != 0 )
		{
			ostringstream nm;
			nm << dirname << "/" << entry->d_name << "/device"; // driver
			if( dir_exist(nm.str()) )
			{
				SerialInfo si;		
//				si.port	= port;
//				si.irq 	= irq;
//				si.uart	= uart;
				si.dev 	= "/dev/" + string(entry->d_name);
				ostringstream dnm;
				dnm << "COM" << num+1;
				si.name = dnm.str();

				lst.push_front(si);
				ideb[Debug::INFO] << "(read_serial): add to list " << si.name << endl;
			}
		}
	}
	closedir(dir);
}
//------------------------------------------------------------------------------
bool Imitator::dir_exist( const string dirname )
{
	DIR *dir;
	dir = opendir(dirname.c_str());
	if( dir == NULL )
		return false;

	closedir (dir);
	return true;
}
//------------------------------------------------------------------------------
std::list<SerialInfo> Imitator::get_serial_devices()
{
	std::list<SerialInfo> lst;
	read_usbserial(lst);
	read_serial(lst);
	return lst;
}
//------------------------------------------------------------------------------
