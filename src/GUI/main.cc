/***************************************************************************
* This file is part of the Imitator project                                *
***************************************************************************/

#include <sstream>
#include <gtkmm/main.h>
#include <Exceptions.h>
#include <Configuration.h>
#include <ObjectsActivator.h>
#include <RunLock.h>
#include <libglademm.h>
//#include <locale.h>
#include "ImitatorTypes.h"
#include "GUIManager.h"


using namespace std;
using namespace Imitator;
DebugStream Imitator::ideb;

int main(int argc, char *argv[])
{
/*
	list<SerialInfo> lst = get_serial_devices();
	if( lst.empty() )
		cout << "Not found serial devices..." << endl;

	for( list<SerialInfo>::const_iterator it=lst.begin();it!=lst.end();++it)
		cout << it->name << ": " << it->dev << endl;

	return 0;
*/

//	char *old=setlocale(LC_NUMERIC, "C");
  try
  {	
  	string confile( UniSetTypes::getArgParam("--confile",argc,argv) );
	if( confile.empty() )
		confile = string(IMITATOR_CONFDIR) + "imitator.xml";

  	UniSetTypes::conf = new UniSetTypes::Configuration(argc, argv, confile);

	
	// Настройка логов
	ostringstream logname;
	string dir(UniSetTypes::conf->getLogDir());
//	string pname(argv[0]);
//	logname << dir << pname << ".log";
	logname << dir << "imitator.log";
	Imitator::ideb.logFile( logname.str().c_str() );
	UniSetTypes::unideb.logFile( logname.str().c_str() );
	UniSetTypes::conf->initDebug(Imitator::ideb,"ImitatorDebug");
	// ---

	// формируем имя для lock-файла
	string login(getenv("LOGNAME"));
	string lockfile(UniSetTypes::conf->getLockDir()+"imitator."+login);
	RunLock rl;
	if( !rl.lock(lockfile) )
	{
		Imitator::ideb[Debug::WARN] << "Программа уже запущена " << endl;
		return 0;
  	}

	Gtk::Main kit(argc, argv);

	Glib::RefPtr<Gnome::Glade::Xml> refXml = Gnome::Glade::Xml::create( UniSetTypes::conf->getDataDir() + "/gui.glade" );

  	Gtk::Window *MainWindow = 0;
	refXml->get_widget("window_main", MainWindow);
	if( MainWindow == 0 )
		exit(-1);
		
	UniSetTypes::ObjectId guid = UniSetTypes::conf->oind->getIdByName(UniSetTypes::conf->getObjectsSection() + "/GUIManager");
	if( guid == UniSetTypes::DefaultObjectId )
	{
		cerr << "(main): В " << confile << " не найден идентификатор GUIManager\n";
		return -1;
	}
	
	GUIManager gmng(guid, refXml);

	ObjectsActivator act;
	act.addManager(static_cast<class ObjectsManager*>(&gmng));
	act.run(true);

	Gtk::Main::run(*MainWindow); //Shows the window and returns when it is closed.
  }
  catch( UniSetTypes::Exception& ex )
  {
		cout << "main: " << ex << endl;
  }
	catch(...)
	{
		cerr << "(main): catch ..." << endl;
	}

  return 0;
}
