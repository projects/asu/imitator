// File:        This file is part of Imitator Project
// Copyright:   2005 Etersoft All rights reserved.
// $Id: GUIManager.cc,v 1.34 2007/09/15 14:19:19 pv Exp $
// -----------------------------------------------------------------------------------------
// TODO:
//  Формирование сообщений не должно происходить в этом файле (Lav 10.07.2005)

#include "GUIManager.h"
#include "ImitatorTypes.h"
#include "TypeFormats.h"
#include "ExternalPacket.h"
#include "Messenger.h"
#include <locale.h>
#include <math.h>

using namespace UniSetTypes;
using namespace Imitator;
using namespace std;

//----------------------------------------------------------------------------------------

GUIManager::GUIManager( ObjectId id, Glib::RefPtr<Gnome::Glade::Xml> &refXml ):
	ObjectsManager(id),
	refXml(refXml),
	combo_send(0),
	combo_recv(0),
	frame_Sigma40(0),
	frame_GPS(0),
	frame_RDL3(0)
{   
//	char *old=setlocale(LC_NUMERIC, "C");
	id_gui = UniSetTypes::conf->getObjectID("GUIMessenger");
	id_send = UniSetTypes::conf->getObjectID("SendServer");
	id_recv = UniSetTypes::conf->getObjectID("RecvServer");
	id_rdl3 = UniSetTypes::conf->getObjectID("RDL3Server");

/*
	{
 	 	window_send_opt = 0;
		refXml->get_widget("window_send_opt", window_send_opt);
		if( window_send_opt == 0 )
			exit(-1);
		window_send_opt->hide();
	}
*/

 	Gtk::HBox* send_box = 0;
	refXml->get_widget("send_box", send_box);
	if( send_box == 0 )
		exit(-1);

 	Gtk::HBox* recv_box = 0;
	refXml->get_widget("recv_box", recv_box);
	if( recv_box == 0 )
		exit(-1);

	{
		combo_send_dev = manage(new class Gtk::ComboBoxEntryText());
		combo_send_dev->signal_changed().connect(sigc::mem_fun(*this, &GUIManager::on_combobox_send_changed));
		combo_send_dev->show();
		send_box->add(*combo_send_dev);
	}

	{
		combo_recv_dev = manage(new class Gtk::ComboBoxEntryText());
		combo_recv_dev->signal_changed().connect(sigc::mem_fun(*this, &GUIManager::on_combobox_recv_changed));
		combo_recv_dev->show();
		recv_box->add(*combo_recv_dev);
	}

/*
	{
 	 	combo_s_dev = 0;
		refXml->get_widget("comboboxentry_s_dev", combo_s_dev);
		if( combo_s_dev == 0 )
			exit(-1);
		combo_s_dev->get_entry()->set_text("/dev/ttyS0");
	}
	{
 	 	combo_s_speed = 0;
		refXml->get_widget("comboboxentry_s_speed", combo_s_speed);
		if( combo_s_speed == 0 )
			exit(-1);
		combo_s_speed->get_entry()->set_text("19200");
	}
	{
 	 	combo_s_byte = 0;
		refXml->get_widget("comboboxentry_s_byte", combo_s_byte);
		if( combo_s_byte == 0 )
			exit(-1);
		combo_s_byte->get_entry()->set_text("8");
	}
	{
 	 	combo_s_parity = 0;
		refXml->get_widget("comboboxentry_s_parity", combo_s_parity);
		if( combo_s_parity == 0 )
			exit(-1);
		combo_s_parity->get_entry()->set_text("N");
	}
	{
 	 	combo_s_stopb = 0;
		refXml->get_widget("comboboxentry_s_stopb", combo_s_stopb);
		if( combo_s_stopb == 0 )
			exit(-1);
		combo_s_stopb->get_entry()->set_text("1");
	}
	{
 	 	combo_s_period = 0;
		refXml->get_widget("comboboxentry_s_period", combo_s_period);
		if( combo_s_period == 0 )
			exit(-1);
		combo_s_period->get_entry()->set_text("100");
	}
*/
	// frame_Sigma40
	{
		refXml->get_widget("frame_Sigma40", frame_Sigma40);
		if( frame_Sigma40 == 0 )
			exit(-1);
	}
	// frame_GPS
	{
		refXml->get_widget("frame_GPS", frame_GPS);
		if( frame_GPS == 0 )
			exit(-1);
	}

	// frame_RDL3
	{
		refXml->get_widget("frame_RDL3", frame_RDL3);
		if( frame_RDL3 == 0 )
			exit(-1);
	}

	// send_start
	{
		button_send_start = 0;
		refXml->get_widget("button_send_start", button_send_start);
		if( button_send_start == 0 )
			exit(-1);

		button_send_start->signal_clicked().connect(sigc::mem_fun(*this, &GUIManager::on_toolbutton_send_start_activate));
	}


	// send_stop
	{
		button_send_stop = 0;
		refXml->get_widget("button_send_stop", button_send_stop);
		if( button_send_stop == 0 )
			exit(-1);

		button_send_stop->signal_clicked().connect(sigc::mem_fun(*this, &GUIManager::on_toolbutton_send_stop_activate));
		button_send_start->modify_bg(Gtk::STATE_NORMAL, Gdk::Color("light gray"));
		button_send_start->modify_bg(Gtk::STATE_PRELIGHT, Gdk::Color("light gray"));
		button_send_stop->modify_bg(Gtk::STATE_NORMAL, Gdk::Color("red"));
		button_send_stop->modify_bg(Gtk::STATE_PRELIGHT, Gdk::Color("red"));
	}

	// recv_start
	{
		button_recv_start = 0;
		refXml->get_widget("button_recv_start", button_recv_start);
		if( button_recv_start == 0 )
			exit(-1);

		button_recv_start->signal_clicked().connect(sigc::mem_fun(*this, &GUIManager::on_toolbutton_recv_start_activate));
	}


	// recv_stop
	{
		button_recv_stop = 0;
		refXml->get_widget("button_recv_stop", button_recv_stop);
		if( button_recv_stop == 0 )
			exit(-1);

		button_recv_stop->signal_clicked().connect(sigc::mem_fun(*this, &GUIManager::on_toolbutton_recv_stop_activate));
		button_recv_start->modify_bg(Gtk::STATE_NORMAL, Gdk::Color("light gray"));
		button_recv_start->modify_bg(Gtk::STATE_PRELIGHT, Gdk::Color("light gray"));
		button_recv_stop->modify_bg(Gtk::STATE_NORMAL, Gdk::Color("red"));
		button_recv_stop->modify_bg(Gtk::STATE_PRELIGHT, Gdk::Color("red"));
	}

/*
	// button_send_opt
	{
		Gtk::ToolButton *button_send_opt = 0;
		refXml->get_widget("toolbutton_send_opt", button_send_opt);
		if( button_send_opt == 0 )
			exit(-1);

		button_send_opt->signal_clicked().connect(sigc::mem_fun(*this, &GUIManager::on_toolbutton_send_opt_activate));
	}

	// button_recv_opt
	{
		Gtk::ToolButton *button_recv_opt = 0;
		refXml->get_widget("toolbutton_recv_opt", button_recv_opt);
		if( button_recv_opt == 0 )
			exit(-1);

		button_recv_opt->signal_clicked().connect(sigc::mem_fun(*this, &GUIManager::on_toolbutton_recv_opt_activate));
	}

	// button_send_confirm_opt
	{
		Gtk::Button *button_s_confirm_opt = 0;
		refXml->get_widget("button_s_confirm_opt", button_s_confirm_opt);
		if( button_s_confirm_opt == 0 )
			exit(-1);

		button_s_confirm_opt->signal_clicked().connect(sigc::mem_fun(*this, &GUIManager::on_button_send_confirm_opt_activate));
	}
*/
	// combo_send
	{
		refXml->get_widget("combobox_send", combo_send);
		if( combo_send == 0 )
			exit(-1);

		combo_send->signal_changed().connect(sigc::mem_fun(*this, &GUIManager::on_combobox_send_changed));
		combo_send->set_active(Sigma40_s);
	}

	// combo_recv
	{
		refXml->get_widget("combobox_recv", combo_recv);
		if( combo_recv == 0 )
			exit(-1);

		combo_recv->signal_changed().connect(sigc::mem_fun(*this, &GUIManager::on_combobox_recv_changed));
		combo_recv->set_active(Sigma40_r);
	}

	// scrollwindow
	Messenger *messenger = manage(new class Messenger(id_gui, "Информация"));
	messenger->show();
	this->addObject(messenger);

	sb = manage(new class Gtk::Statusbar());
	sb->show();

	{
		Gtk::VBox* widget = 0;
		refXml->get_widget("vbox5", widget);
		if( widget == 0 )
			exit(-1);
		widget->pack_start(*messenger,true, true);
		widget->pack_start(*sb, false, false);
	}
	detect_devices();
	if(serlist.size())
	{
		list<SerialInfo>::const_iterator it = serlist.begin();
		combo_send_dev->set_active_text(it->name);
		if(serlist.size() >=2)
			combo_recv_dev->set_active_text((++it)->name);
		else
			combo_recv_dev->set_active_text(it->name);
	}

	Gtk::Window* mwin = 0;
	if( !mwin )
		refXml->get_widget("window_main", mwin);

	if( mwin && UniSetTypes::findArgParam("--maximize", conf->getArgc(),conf->getArgv()) != -1 )
	{
		mwin->move(0,0);
		mwin->maximize();
//		mwin->fullscreen();
	}

	if( mwin && UniSetTypes::findArgParam("--fullscreen", conf->getArgc(),conf->getArgv()) != -1 )
		mwin->fullscreen();
}

//----------------------------------------------------------------------------------------

GUIManager::~GUIManager()
{   
}

//----------------------------------------------------------------------------------------

void GUIManager::on_toolbutton_send_start_activate()
{

	ideb[Debug::INFO] << myname << " call on_toolbutton_send_start_activate" << endl;

	button_send_start->modify_bg(Gtk::STATE_NORMAL, Gdk::Color("green"));
	button_send_start->modify_bg(Gtk::STATE_PRELIGHT, Gdk::Color("green"));
	button_send_stop->modify_bg(Gtk::STATE_NORMAL, Gdk::Color("light gray"));
	button_send_stop->modify_bg(Gtk::STATE_PRELIGHT, Gdk::Color("light gray"));
	

	int index = combo_send->get_active_row_number();
//	char *old=setlocale(LC_NUMERIC, "C");

	if(index == Sigma40_s)
	{
		Sigma40Packet pckt;
		int size = sizeof(pckt);
		memset(&pckt, 0, size);
		
		pckt.header1 = 0x5A;
		pckt.header2 = 0xA5;
		pckt.numdata = 0x48;
		pckt.ident = 0x2;
		pckt.terminator = 0xAA;
		
		double heading, roll, pitch, heading_rate;

		{
			Gtk::SpinButton *widget = 0;
			refXml->get_widget("spinbutton_heading", widget);
			if( widget == 0 )
				exit(-1);
			heading = widget->get_value();
			pckt.heading = round(heading * (1 << 16) / 360);
		}
		{
			Gtk::SpinButton *widget = 0;
			refXml->get_widget("spinbutton_roll", widget);
			if( widget == 0 )
				exit(-1);
			roll = widget->get_value();
			pckt.roll = round(roll * (1 << 15) / 90);
		}
		{
			Gtk::SpinButton *widget = 0;
			refXml->get_widget("spinbutton_pitch", widget);
			if( widget == 0 )
				exit(-1);
			pitch = widget->get_value();
			pckt.pitch = round(pitch * (1 << 15) / 90);
		}
		{
			Gtk::SpinButton *widget = 0;
			refXml->get_widget("spinbutton_heading_rate", widget);
			if( widget == 0 )
				exit(-1);
			heading_rate = widget->get_value();
			pckt.heading_rate = round(heading_rate * (1 << 15));
		}

		int cs = 0;
		char buf[sizeof(pckt)];
		memcpy(buf, &pckt, sizeof(buf));
		for(int i = 2; i < size - 2; i++)
			cs = cs + buf[i];
		pckt.checkbyte = cs;

		if( id_send != DefaultObjectId )
		{
			try
			{
				TransportMessage tm(CommMessage(reinterpret_cast<char*>(&pckt), size, getId()).transport_msg());
				ui.send(id_send, tm);
			}
			catch( UniSetTypes::Exception& ex )
  			{
				ideb[Debug::INFO] << myname << " " << ex << endl;
 			}
		}
		else
			ideb[Debug::INFO] << myname << " Object SendServer not found" << endl;

		if( id_gui != DefaultObjectId )
		{
			int index = combo_recv->get_active_row_number();
			TransportMessage tm;
			if(index == Sigma40_r)
			{
				TransportMessage tm1(CommMessage(reinterpret_cast<char*>(&pckt), size, getId()).transport_msg());
				tm = tm1;
			}	
			else if(index == HDT_r)
			{
				char buf[100];
				string str = "$HEHDT," + precise_float(heading, 3, 2) + ",T*";
				#warning "FIXME: Что за странные манипуляции со строкой в sprintf? И ниже. (Lav 10.07.2005)"
				sprintf(buf,"%s", str.c_str());
//				sprintf(buf, "$HEHDT,%03.02f,T*", heading);
				add_gps_check_sum(buf);
				TransportMessage tm1(CommMessage(buf, gps_msg_sizeof(buf), getId()).transport_msg());
				tm = tm1;
				ideb[Debug::INFO] << myname << " HDT " << buf << endl;
			}	
			else if(index == HDTE_r)
			{
				char buf[100];
				string str = "$HEHDT," + precise_float(heading, 3, 2) + ",T," + precise_float(roll, 2, 2) + "," +
				precise_float(pitch, 2, 2) + "," + precise_float(heading_rate, 1, 3) + "*";
				sprintf(buf,"%s", str.c_str());
//				sprintf(buf, "$HEHDT,%03.02f,T,%+02.02f,%+02.02f,%+1.03f*", heading, roll, pitch, heading_rate);
				add_gps_check_sum(buf);
				TransportMessage tm1(CommMessage(buf, gps_msg_sizeof(buf), getId()).transport_msg());
				tm = tm1;
				ideb[Debug::INFO] << myname << " HDTE " << buf << endl;
			}	
			try
			{
				ui.send(id_gui, tm);
			}
			catch( UniSetTypes::Exception& ex )
  			{
				ideb[Debug::INFO] << myname << " " << ex << endl;
 			}
		}
		else
			ideb[Debug::INFO] << myname << " Object GUIMessenger not found" << endl;
	}
	else if(index == GPS_s)
	{
		char latitude[100];
		char longtitude[100];

		{
			Gtk::SpinButton *widget = 0;
			refXml->get_widget("spinbutton_GPS_L", widget);
			if( widget == 0 )
				exit(-1);
			widget->signal_output();
			get_str_latitude(latitude, widget->get_value());
		}

		{
			Gtk::SpinButton *widget = 0;
			refXml->get_widget("spinbutton_GPS_Y", widget);
			if( widget == 0 )
				exit(-1);
			get_str_longtitude(longtitude, widget->get_value());
		}

		char buf[100];
		memset(buf, 0, sizeof(buf));
		char courseNS;
		char courseWE;
		{
			Gtk::RadioButton *widget = 0;
			refXml->get_widget("butN", widget);
			if( widget == 0 )
				exit(-1);
			if( widget->get_active() )
				courseNS = 'N';
			else
				courseNS = 'S';
		}

		{
			Gtk::RadioButton *widget = 0;
			refXml->get_widget("butW", widget);
			if( widget == 0 )
				exit(-1);
			if( widget->get_active() )
				courseWE = 'W';
			else
				courseWE = 'E';
		}
			
		{
			Gtk::RadioButton *widget = 0;
			refXml->get_widget("butGGA", widget);
			if( widget == 0 )
				exit(-1);
			if(widget->get_active())
			{
				ideb[Debug::INFO] << "GGA" << endl;
				sprintf(buf, "$GPGGA,000000,%s,%c,%s,%c,1,2,00.00,,M,,M,,0000*", latitude, courseNS,
																			longtitude, courseWE);
			}
		}
		{
			Gtk::RadioButton *widget = 0;
			refXml->get_widget("butRMC", widget);
			if( widget == 0 )
				exit(-1);
			if(widget->get_active())
			{
				ideb[Debug::INFO] << "RMC" << endl;
				sprintf(buf, "$GPRMC,000000.00,A,%s,%c,%s,%c,12.34,056.78,010104,,*", latitude, courseNS,
																			longtitude, courseWE);
			}
		}
		{
			Gtk::RadioButton *widget = 0;
			refXml->get_widget("butGLL", widget);
			if( widget == 0 )
				exit(-1);
			if(widget->get_active())
			{
				ideb[Debug::INFO] << "GLL" << endl;
				sprintf(buf, "$GPGLL,%s,%c,%s,%c,000000,A,*", latitude, courseNS,
																longtitude, courseWE);
			}
		}
		{
			Gtk::RadioButton *widget = 0;
			refXml->get_widget("butVTG", widget);
			if( widget == 0 )
				exit(-1);
			if(widget->get_active())
			{
				ideb[Debug::INFO] << "VTG" << endl;
				sprintf(buf, "$GPVTG,123.45,T,000.0,M,06.78,N,00.00,K*");
			}
		}
	
		add_gps_check_sum(buf);
		if( id_send != DefaultObjectId )
		{
			TransportMessage tm(CommMessage(buf, gps_msg_sizeof(buf), getId()).transport_msg());
			try
			{
				ideb[Debug::INFO] << myname << " " << buf << endl;
				ui.send(id_send, tm);
			}
			catch( UniSetTypes::Exception& ex )
  			{
				ideb[Debug::INFO] << myname << " " << ex << endl;
 			}
			try
			{
				ideb[Debug::INFO] << myname << " " << buf << endl;
				if( id_gui != DefaultObjectId )
					ui.send(id_gui, tm);
			}
			catch( UniSetTypes::Exception& ex )
  			{
				ideb[Debug::INFO] << myname << " " << ex << endl;
 			}
		}
		else
			ideb[Debug::INFO] << myname << " Object SendServer not found" << endl;

	}
	else if(index == RDL3_s)
	{
		double Vx,Vy;
		RDL3Packet pckt;
		{
			Gtk::SpinButton *widget = 0;
			refXml->get_widget("spinbutton_Vx", widget);
			if( widget == 0 )
				exit(-1);
			Vx = widget->get_value();
			pckt.Vx = round(Vx * 100.);
		}
		{
			Gtk::SpinButton *widget = 0;
			refXml->get_widget("spinbutton_Vy", widget);
			if( widget == 0 )
				exit(-1);
			Vy = widget->get_value();
			pckt.Vy = round(Vy * 100.);
		}
		{
			Gtk::ToggleButton *widget = 0;
			refXml->get_widget("butDenied", widget);
			if( widget == 0 )
				exit(-1);
			pckt.denied = widget->get_active();
		}
		{
			Gtk::ToggleButton *widget = 0;
			refXml->get_widget("butCheck", widget);
			if( widget == 0 )
				exit(-1);
			pckt.check = widget->get_active();
		}
		
		if( id_rdl3 != DefaultObjectId )
		{
			try
			{
				TransportMessage tm(CommMessage(reinterpret_cast<char*>(&pckt), sizeof(pckt), getId()).transport_msg());
				ui.send(id_rdl3, tm);
			}
			catch( UniSetTypes::Exception& ex )
  			{
				ideb[Debug::INFO] << myname << " " << ex << endl;
 			}
			char buf[100];
			
			string check;
			if(pckt.check)
				check="V";
			else
				check="A";
			string str = "$VDVBW," + precise_float(Vx, 2, 2) + "," + precise_float(Vy, 2, 2) + "," + check + ",,,*";
			sprintf(buf,"%s", str.c_str());
//			sprintf(buf, "$VDVBW,%02.02f,%+02.02f,a,,,*", Vx, Vy);
			add_gps_check_sum(buf);
			ideb[Debug::INFO] << myname << " VDVBW " << buf << endl;
			try
			{
				if( id_gui != DefaultObjectId )
				{
					TransportMessage tm(CommMessage(buf, gps_msg_sizeof(buf), getId()).transport_msg());
					ui.send(id_gui, tm);
				}
			}
			catch( UniSetTypes::Exception& ex )
  			{
				ideb[Debug::INFO] << myname << " " << ex << endl;
 			}
		}
		else
			ideb[Debug::INFO] << myname << " Object RDL3Server not found" << endl;
	}
//	setlocale(LC_NUMERIC, old);
}

//----------------------------------------------------------------------------------------

void GUIManager::on_toolbutton_send_stop_activate()
{
	detect_devices();
	ideb[Debug::INFO] << myname << " call on_toolbutton_send_stop_activate" << endl;

	button_send_start->modify_bg(Gtk::STATE_NORMAL, Gdk::Color("light gray"));
	button_send_start->modify_bg(Gtk::STATE_PRELIGHT, Gdk::Color("light gray"));
	button_send_stop->modify_bg(Gtk::STATE_NORMAL, Gdk::Color("red"));
	button_send_stop->modify_bg(Gtk::STATE_PRELIGHT, Gdk::Color("red"));

	ConfMessage cfmsg_s(ConfMessage::Stop);
	TransportMessage tm(cfmsg_s.transport_msg());
	{
		if( id_send != DefaultObjectId )
		{
			try
			{
				ui.send(id_send, tm);
			}
			catch( UniSetTypes::Exception& ex )
			{
				ideb[Debug::INFO] << myname << " " << ex << endl;
			}
		}
		else
			ideb[Debug::INFO] << myname << " Object SendServer not found" << endl;
	}
	{
		if( id_rdl3 != DefaultObjectId )
		{
			try
			{
				ui.send(id_rdl3, tm);
			}
			catch( UniSetTypes::Exception& ex )
			{
				ideb[Debug::INFO] << myname << " " << ex << endl;
			}
		}
		else
			ideb[Debug::INFO] << myname << " Object RDL3Server not found" << endl;
	}
	on_combobox_send_changed();
}

//----------------------------------------------------------------------------------------

void GUIManager::on_toolbutton_recv_start_activate()
{
	ideb[Debug::INFO] << myname << " call on_toolbutton_recv_start_activate" << endl;
	on_combobox_recv_changed();

	button_recv_start->modify_bg(Gtk::STATE_NORMAL, Gdk::Color("green"));
	button_recv_start->modify_bg(Gtk::STATE_PRELIGHT, Gdk::Color("green"));
	button_recv_stop->modify_bg(Gtk::STATE_NORMAL, Gdk::Color("light gray"));
	button_recv_stop->modify_bg(Gtk::STATE_PRELIGHT, Gdk::Color("light gray"));

	if( id_recv != DefaultObjectId )
	{
		ConfMessage cfmsg_s(ConfMessage::Start);
		TransportMessage tm(cfmsg_s.transport_msg());

		try
		{
			ui.send(id_recv, tm);
		}
		catch( UniSetTypes::Exception& ex )
		{
			ideb[Debug::INFO] << myname << " " << ex << endl;
		}
	}
	else
		ideb[Debug::INFO] << myname << " Object RecvServer not found" << endl;
}

//----------------------------------------------------------------------------------------

void GUIManager::on_toolbutton_recv_stop_activate()
{
	detect_devices();
	ideb[Debug::INFO] << myname << " call on_toolbutton_recv_stop_activate" << endl;

	button_recv_start->modify_bg(Gtk::STATE_NORMAL, Gdk::Color("light gray"));
	button_recv_start->modify_bg(Gtk::STATE_PRELIGHT, Gdk::Color("light gray"));
	button_recv_stop->modify_bg(Gtk::STATE_NORMAL, Gdk::Color("red"));
	button_recv_stop->modify_bg(Gtk::STATE_PRELIGHT, Gdk::Color("red"));

	if( id_recv != DefaultObjectId )
	{
		ConfMessage cfmsg_s(ConfMessage::Stop);
		TransportMessage tm(cfmsg_s.transport_msg());

		try
		{
			ui.send(id_recv, tm);
		}
		catch( UniSetTypes::Exception& ex )
		{
			ideb[Debug::INFO] << myname << " " << ex << endl;
		}
	}
	else
		ideb[Debug::INFO] << myname << " Object RecvServer not found" << endl;
	
	on_combobox_recv_changed();
}

//----------------------------------------------------------------------------------------

void GUIManager::on_combobox_send_changed()
{
	ideb[Debug::INFO] << myname << " call on_combobox_send_changed" << endl;
	int index = combo_send->get_active_row_number();
	ideb[Debug::INFO] << myname << " selected index=" << index << endl;

	Glib::ustring dev = combo_send_dev->get_entry()->get_text();

	for(list<SerialInfo>::const_iterator it=serlist.begin();it!=serlist.end();++it)
	{
		if(it->name == dev)
			dev = it->dev;
	}

	if(dev == combo_send_dev->get_entry()->get_text())
	{
		ideb[Debug::WARN] << myname << " selected send device not found!!! set /dev/null" << endl;
		dev = "/dev/null";
	}

	ConfMessage cfmsg_s(ConfMessage::Config);

	if(index == Sigma40_s)
	{
		frame_Sigma40->show();
		frame_GPS->hide();
		frame_RDL3->hide();
		cfmsg_s.ConfigComPort(dev, ComPort::ComSpeed19200, ComPort::Even, ComPort::CSize8, ComPort::OneBit, 100, 50000);
	}	
	else if(index == GPS_s)
	{
		cfmsg_s.ConfigComPort(dev, ComPort::ComSpeed4800, ComPort::NoParity, ComPort::CSize8, ComPort::OneBit, 1000, 50000);
		frame_Sigma40->hide();
		frame_GPS->show();
		frame_RDL3->hide();
	}	
	else if(index == RDL3_s)
	{
		frame_Sigma40->hide();
		frame_GPS->hide();
		frame_RDL3->show();
		cfmsg_s.ConfigLptPort("/dev/lp0", 2000);
		if( id_rdl3 != DefaultObjectId)
		{
			try
			{
				TransportMessage tm(cfmsg_s.transport_msg());
				ui.send(id_rdl3, tm);
			}
			catch( UniSetTypes::Exception& ex )
			{
				ideb[Debug::INFO] << myname << " " << ex << endl;
 			}
		}
		else
			ideb[Debug::INFO] << myname << " Object RDL3Server not found" << endl;
		return;
	}

	if( id_send != DefaultObjectId)
	{
		try
		{
			TransportMessage tm(cfmsg_s.transport_msg());
			ideb[Debug::INFO] << myname << " Sending..." << endl;
			ui.send(id_send, tm);
			ideb[Debug::INFO] << myname << " Sending OK" << endl;
		}
		catch( UniSetTypes::Exception& ex )
		{
			ideb[Debug::INFO] << myname << " " << ex << endl;
		}
	}
	else
		ideb[Debug::INFO] << myname << " Object SendServer not found" << endl;

}

//----------------------------------------------------------------------------------------

void GUIManager::on_combobox_recv_changed()
{
	ideb[Debug::INFO] << myname << " call on_comboboxentry_recv_changed()" << endl;
	int index = combo_recv->get_active_row_number();
	ideb[Debug::INFO] << myname << " selected index=" << index << endl;

	Glib::ustring dev = combo_recv_dev->get_entry()->get_text();

	for(list<SerialInfo>::const_iterator it=serlist.begin();it!=serlist.end();++it)
	{
		if(it->name == dev)
			dev = it->dev;
	}

	if(dev == combo_recv_dev->get_entry()->get_text())
	{
		ideb[Debug::WARN] << myname << " selected recv device not found!!! set /dev/null" << endl;
		dev = "/dev/null";
	}
	
	ConfMessage cfmsg_r(ConfMessage::Config);

	if(index == Sigma40_r)
	{
		cfmsg_r.ConfigComPort(dev, ComPort::ComSpeed19200, ComPort::Even, ComPort::CSize8, ComPort::OneBit, 100, 50000);
	}	
	else if(index == GPS_r)
	{
		cfmsg_r.ConfigComPort(dev, ComPort::ComSpeed4800, ComPort::NoParity, ComPort::CSize8, ComPort::OneBit, 1000, 50000);
	}	
	else if(index == HDT_r)
	{
		cfmsg_r.ConfigComPort(dev, ComPort::ComSpeed4800, ComPort::NoParity, ComPort::CSize8, ComPort::OneBit, 100, 50000);
	}	
	else if(index == HDTE_r)
	{
		cfmsg_r.ConfigComPort(dev, ComPort::ComSpeed4800, ComPort::NoParity, ComPort::CSize8, ComPort::OneBit, 100, 50000);
	}	
	else if(index == VBW_r)
	{
		cfmsg_r.ConfigComPort(dev, ComPort::ComSpeed4800, ComPort::NoParity, ComPort::CSize8, ComPort::OneBit, 1000, 50000);
	}	

	{
		if( id_recv != DefaultObjectId )
		{
			try
			{
				TransportMessage tm(cfmsg_r.transport_msg());
				ui.send(id_recv, tm);
			}
			catch( UniSetTypes::Exception& ex )
			{
				ideb[Debug::INFO] << myname << " " << ex << endl;
 			}
		}
		else
			ideb[Debug::INFO] << myname << " Object RecvServer not found" << endl;
	}
}

//----------------------------------------------------------------------------------------

void GUIManager::get_str_latitude(char *buf, double latit)
{
	ideb[Debug::INFO] << myname << "::get_str_latitude, latit=" << latit << endl;
	bool negative = (latit < 0);
	if (negative)
		latit = -latit;
	int deg = (int)latit;
	double min = ((latit-deg)*60.);
	assert( deg <= 90 );
	string str1(precise_float(deg, 2, 0));
	string str2(precise_float(min, 2, 4));
	str1 = str1 + str2;
	sprintf(buf,"%s", str1.c_str());
}

//----------------------------------------------------------------------------------------

void GUIManager::get_str_longtitude(char *buf, double longt)
{
	ideb[Debug::INFO] << myname << "::get_str_longtitude, longt=" << longt << endl;
	bool negative = (longt < 0);
	if (negative)
		longt = -longt;
	int deg = (int)longt;
	double min = ((longt-deg)*60.);
	assert( deg <= 180 );
	string str1(precise_float(deg, 3, 0));
	string str2(precise_float(min, 2, 4));
	str1 = str1 + str2;
	sprintf(buf,"%s", str1.c_str());
}

//----------------------------------------------------------------------------------------

// FIXME: контрольная сумма вообще-то должна считаться для указанного количества байт
void GUIManager::add_gps_check_sum(char *buf)
{
	int cs = 0;
	for(int i = 1; buf[i] != '*' && buf[i]; i++)
		cs ^= buf[i];
	sprintf(buf,"%s%X\r\n", buf, cs);
}

//----------------------------------------------------------------------------------------

int GUIManager::gps_msg_sizeof(char *buf)
{
	string str(buf);
	return str.size();
}

//----------------------------------------------------------------------------------------
/*
void GUIManager::on_toolbutton_send_opt_activate()
{
	window_send_opt->show();
}

//----------------------------------------------------------------------------------------

void GUIManager::on_toolbutton_recv_opt_activate()
{
}

//----------------------------------------------------------------------------------------

void GUIManager::on_button_send_confirm_opt_activate()
{
	window_send_opt->hide();
}
*/

void GUIManager::processingMessage( UniSetTypes::VoidMessage *msg )
{
	try
	{
		ideb[Debug::INFO] << myname << " processingMessage " << endl;
		switch( msg->type )
		{
/*
			case Message::SensorInfo:
			{
				SensorMessage sm( msg );
				sensorInfo( &sm );
				break;
			}
*/
			case Message::SysCommand:
			{
				SystemMessage sm( msg );
				if(sm.command == SystemMessage::LogRotate)
				{
					{
						string fname = ideb.getLogFile();
						if( !fname.empty() )
						  ideb.logFile(fname.c_str());
					}
					{
						string fname = unideb.getLogFile();
						if( !fname.empty() )
						  unideb.logFile(fname.c_str());
					}
				}
				else if(sm.command == SystemMessage::ReConfiguration)
				{
					on_combobox_send_changed();
					on_combobox_recv_changed();
				}
				break;
			}
		}
	}
	catch(Exception& ex)
	{
//		ideb[Debug::CRIT]  << myname << "(processingMessage): " << ex << endl;
	}
}
// ------------------------------------------------------------------------------------------

void GUIManager::detect_devices()
{
	serlist = get_serial_devices();
	combo_send_dev->clear();
	combo_recv_dev->clear();
	if(!serlist.empty())
		for( list<SerialInfo>::const_iterator it=serlist.begin();it!=serlist.end();++it)
		{
			combo_send_dev->append_text(it->name);
			combo_recv_dev->append_text(it->name);
			ideb[Debug::INFO] << myname << ":detect_devices " << it->name << ":\t" << it->dev << endl;
		}
}
