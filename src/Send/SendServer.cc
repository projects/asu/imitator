#include <Configuration.h>
#include "SendServer.h"
// --------------------------------------------------------------------------------
using namespace UniSetTypes;
using namespace std;
// --------------------------------------------------------------------------------

SendServer::SendServer( ObjectId id ):
UniSetObject_LT(id)
{
}

// --------------------------------------------------------------------------------

SendServer::~SendServer()
{
}

// --------------------------------------------------------------------------------
void SendServer::processingMessage( UniSetTypes::VoidMessage *msg )
{
	try
	{
		switch( msg->type )
		{
			case Message::SensorInfo:
			{
				SensorMessage sm( msg );
				sensorInfo( &sm );
				break;
			}

			case Message::SysCommand:
			{
				SystemMessage sm( msg );
				sysCommand( &sm );
				break;
			}

			case Message::Timer:
			{
				TimerMessage tm(msg);
				timerInfo(&tm);
				break;
			}

			default:
//				ideb[Debug::WARN] << myname << ": неизвестное сообщение  " << msg->type << endl;
				break;
		}	
	}
	catch(Exception& ex)
	{
//		ideb[Debug::CRIT]  << myname << "(processingMessage): " << ex << endl;
	}
}
// ------------------------------------------------------------------------------------------
void SendServer::sensorInfo( SensorMessage *sm )
{
}
// ------------------------------------------------------------------------------------------
void SendServer::timerInfo( TimerMessage *tm )
{
}
// ------------------------------------------------------------------------------------------
void SendServer::sysCommand( SystemMessage *sm )
{
	switch( sm->command )
	{
		case SystemMessage::StartUp:
		{
			askSensors(UniversalIO::UIONotify);
			break;
		}				
		
		case SystemMessage::FoldUp:								
		case SystemMessage::Finish:
			askSensors(UniversalIO::UIODontNotify);
			break;
		
		case SystemMessage::WatchDog:
			askSensors(UniversalIO::UIONotify);
			break;

		default:
			break;
	}
}

// ------------------------------------------------------------------------------------------
void SendServer::askSensors( UniversalIO::UIOCommand cmd )
{
//	try
//	{
//	}
//	catch( Exception& ex )
//	{
//		ideb[Debug::CRIT] << myname << "(askSensors): " << ex << endl;
//	}
}
// ------------------------------------------------------------------------------------------

