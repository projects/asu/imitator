#ifndef SendServer_H_
#define SendServer_H_

#include <string>
#include <UniSetObject_LT.h>

class SendServer:
	public UniSetObject_LT
{
	public:
		SendServer( UniSetTypes::ObjectId id );
		virtual ~SendServer();

	protected:
		// Функции обработки пришедших сообщений 
		virtual void processingMessage( UniSetTypes::VoidMessage *msg );
		void askSensors( UniversalIO::UIOCommand cmd );
		void sysCommand( UniSetTypes::SystemMessage *sm );
		void sensorInfo( UniSetTypes::SensorMessage *sm );
		void timerInfo( UniSetTypes::TimerMessage *tm );

	private:

};

#endif // SendServer_H_
